const path = require("path");
const fs = require('fs');
const webpack = require('webpack');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

module.exports = {
    entry: "./src/index.tsx",
    devtool: 'inline-source-map',
    output: {
        filename: 'main.js',
        publicPath:  '/'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|ts|tsx)$/,
                exclude: /node_modules/,
                use: "babel-loader",
                include: /src/
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.html$/,
                loader: "html-loader"
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                  "style-loader",
                  "css-loader",
                  "sass-loader",
                ],
              },
            {
                test: /\.(png|jpe?g|gif|svg|jp2|webp)$/,
                loader: 'file-loader',
                options: {
                    limit: 10000,
                    name: 'assets/[name].[ext]'
                },
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            pkg: require("./package.json"),
            template: "./templates/client/index.html"
        }),
        new webpack.ProvidePlugin({
            "React": "react",
        }),
    ],
    devServer: {
        port: 3000,
        hot: true
    },
    resolve: {
        alias: {
            '@app': resolveApp('src'),
            '@assets': resolveApp('assets'),
        },
        extensions: ['.ts', '.tsx', '.js', '.json']
    },
    mode: 'development'
};
