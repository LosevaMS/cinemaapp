import "@app/AppStyle.css"
import { Provider }  from 'mobx-react'
import rootStore from '@app/store/RootStore'
import Routers from '@app/Routers'


function App() {

    return (
        <>
            <Provider rootStore={rootStore}>
                <Routers />
            </Provider>
        </>
    );
}

export default App;