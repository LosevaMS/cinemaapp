import React, { useEffect, useState } from 'react'
import { observer, inject } from 'mobx-react'
import './Profile.css'
import { doReLogin } from '@app/store/UserStore/actions';
import { getRequest, putRequest } from '@app/utils'
import { AUTH_URL } from '@app/constants'

const Profile = ({ rootStore }: any) => {

    const [image, setImage] = useState({});
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [user, setUser] = useState('');

    useEffect(() => {
        if (user === '')
            getRequest(AUTH_URL + '/current_user/',
                {
                    Authorization: `JWT ${localStorage.getItem('token')}`
                })
                .then(response => {
                    setUser(response.data.username);
                    setName(response.data.first_name);
                    setEmail(response.data.username);
                })
    })

    const onImageChange = (event: any) => {
        if (event.target.files && event.target.files[0]) {
            setImage({ backgroundImage: `url('${URL.createObjectURL(event.target.files[0])}')` })
        }
    }

    const handleChange = (event: any) => {
        if (event.target.type === 'text') setName(event.target.value);
        if (event.target.type === 'email') setEmail(event.target.value);
        if (event.target.type === 'password') setPassword(event.target.value);
    }

    return (
        <div className='profile-container'>
            <div className='avatar' style={image} title=''>
                <input type="file" onChange={(e) => onImageChange(e)} className="filetype" id="group_image" />
            </div>
            <div className='profile-settings'>
                <h3>Профиль</h3>
                <form >
                    <label>
                        Имя
                        <input type="text" value={name} onChange={(e) => handleChange(e)} />
                    </label>
                    <label>
                        Почта
                        <input type="email" value={email} onChange={(e) => handleChange(e)} />
                    </label>
                    <label>
                        Пароль
                        <input type="password" value={password} onChange={(e) => handleChange(e)} />
                    </label>
                </form>
            </div>
        </div>
    )
}

export default inject('rootStore')(observer(Profile));