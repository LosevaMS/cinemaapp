import React, { useEffect, useState } from 'react'
import MultiselectCheckbox from '@app/components/Pages/Settings/Criteria/MultiselectCheckbox'
import './Criteria.css'

function Criteria() {

    const options1 = [{ label: 'Фильм', checked: (localStorage.getItem('Criteria')?.includes('Фильм') ? true : false), disabled: false },
    { label: 'Адрес кинотеатра', checked: (localStorage.getItem('Criteria')?.includes('Адрес кинотеатра') ? true : false), disabled: false },
    { label: 'Ряд', checked: (localStorage.getItem('Criteria')?.includes('Ряд') ? true : false), disabled: false },
    { label: 'Стоимость билета', checked: (localStorage.getItem('Criteria')?.includes('Стоимость билета') ? true : false), disabled: false },
    { label: 'Жанр', checked: (localStorage.getItem('Criteria')?.includes('Жанр') ? true : false), disabled: false },
    { label: 'Страна производства', checked: (localStorage.getItem('Criteria')?.includes('Страна производства') ? true : false), disabled: false },
    { label: 'Рейтинг', checked: (localStorage.getItem('Criteria')?.includes('Рейтинг') ? true : false), disabled: false },
    { label: 'Актеры', checked: (localStorage.getItem('Criteria')?.includes('Актеры') ? true : false), disabled: false },
    { label: 'Возрастное ограничение', checked: (localStorage.getItem('Criteria')?.includes('Возрастное ограничение') ? true : false), disabled: false },
    { label: 'Время сеанса', checked: (localStorage.getItem('Criteria')?.includes('Время сеанса') ? true : false), disabled: false },
    { label: 'Дата сеанса', checked: (localStorage.getItem('Criteria')?.includes('Дата сеанса') ? true : false), disabled: false },
    { label: 'Формат фильма', checked: (localStorage.getItem('Criteria')?.includes('Формат фильма') ? true : false), disabled: false }];

    let count = 0;
    options1.forEach((elem) => {
        if (elem.checked) count++;
    })
    if (count > 2) {
        options1.forEach((elem) => {
            if (!elem.checked)
                elem.disabled = true;
        })
    } else {
        options1.forEach((elem) => {
            elem.disabled = false;
        })
    }

    const onChange = (data: any) => {
        let criteriaArray: any[] = [];
        data.forEach((elem: any) => {
            criteriaArray.push(elem.label)
        })
        localStorage.setItem('Criteria', JSON.stringify(criteriaArray));
        if (data.length === 0)
            localStorage.setItem('Criteria', JSON.stringify([['Фильм'], ['Адрес кинотеатра'], ['Жанр']]))
    }

    return (
        <div className='criteria-container'>
            <h3>Критерии поиска по умолчанию</h3>
            <div className='criteria-checkbox-flex'>
                <div className='multiselect-div'>
                    <MultiselectCheckbox
                        options={options1}
                        onChange={onChange}
                    /></div>
            </div>
        </div>
    )
}

export default Criteria
