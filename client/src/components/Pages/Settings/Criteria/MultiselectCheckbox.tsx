import React, { useState } from 'react'
import './MultiselectCheckbox.css'
import Checkbox from '@material-ui/core/Checkbox'

const MultiselectCheckbox = ({ options, onChange }: any) => {
    const [data, setData] = useState(options);

    const toggle = (index: any) => {
        const newData = [...data];
        newData.splice(index, 1, {
            label: data[index].label,
            checked: !data[index].checked,
            disabled: data[index].disabled
        });
        setData(newData);
        onChange(newData.filter(x => x.checked));

        let count = 0;
        newData.forEach((elem) => {
            if (elem.checked) count++;
        })
        if (count > 2) {
            newData.forEach((elem) => {
                if (!elem.checked)
                    elem.disabled = true;
            })
        } else {
            newData.forEach((elem) => {
                elem.disabled = false;
            })
        }
    };

    return (
        <>
            {data.map((item: any, index: any) => (
                <label key={item.label} className='checkbox-label'>
                    <Checkbox
                        readOnly
                        disabled={item.disabled}
                        className='checkbox-other'
                        checked={item.checked || false}
                        onChange={() => toggle(index)}
                    />
                    {item.label}
                </label>
            ))}
        </>
    );
};

export default MultiselectCheckbox;