import React, { useEffect, useState } from 'react'
import './Preferences.css'
import MultiSelect from "react-multi-select-component"
import { CriteriaData } from '@app/components/Pages/Search/SearchWidget/CriteriaData'
import PersonsWidget from './PersonsWidget'

const internationalization = {
    "allItemsAreSelected": "Выбрано все",
    "clearSearch": "Очистить поиск",
    "noOptions": "Не найдено",
    "search": "Поиск",
    "selectAll": "Выбрать все",
    "selectSomeItems": "Выбрать..."
}

function Preferences() {

    const getSelectValueFromStorage = (storageKey: string) => {
        if (localStorage.getItem(storageKey)?.length)
            return JSON.parse(localStorage.getItem(storageKey)).map(item => {
                let obj = new Object();
                obj.label = item;
                obj.value = item;
                return obj;
            })
        else return []
    }

    const [genres, setGenres] = useState(getSelectValueFromStorage('favoriteGenres'));
    const [countries, setCountries] = useState(getSelectValueFromStorage('favoriteCountries'));
    const [ageLimits, setAgeLimits] = useState(getSelectValueFromStorage('favoriteAgeLimits'));

    function filterOptions(options: any, filter: any) {
        if (!filter) {
            return options;
        }
        const re = new RegExp(filter, "i");
        return options.filter(({ value }: any) => value && value.match(re));
    }

    const preferences = ['genres', 'countries', 'age_limit'];

    const selectChangeHandle = (e: any, preference: string) => {
        switch (preference) {
            case 'genres':
                setGenres(e);
                let genresArray: any[] = [];
                e.forEach((item: { label: any }) => genresArray.push(item.label))
                localStorage.setItem('favoriteGenres', JSON.stringify(genresArray))
                break;
            case 'countries':
                setCountries(e);
                let countriesArray: any[] = [];
                e.forEach((item: { label: any }) => countriesArray.push(item.label))
                localStorage.setItem('favoriteCountries', JSON.stringify(countriesArray))
                break;
            case 'age_limit':
                setAgeLimits(e);
                let ageLimitsArray: any[] = [];
                e.forEach((item: { label: any }) => ageLimitsArray.push(item.label))
                localStorage.setItem('favoriteAgeLimits', JSON.stringify(ageLimitsArray))
                break;
        }
    }

    return (
        <div className='preferences-container'>
            <h3>Мои предпочтения</h3>
            {preferences.map(item => {
                return (
                    <div className='flex-preferences'>
                        <p>{item === 'genres' ? 'Жанры' : item === 'countries' ? 'Страны производства'
                            : 'Возрастные ограничения'}</p>
                        <MultiSelect
                            key='test'
                            options={item === 'genres' ? CriteriaData[2].options
                                : item === 'countries' ? CriteriaData[4].options
                                    : CriteriaData[3].options}
                            value={item === 'genres' ? genres : item === 'countries' ? countries
                                : ageLimits}
                            onChange={(e: any) => selectChangeHandle(e, item)}
                            labelledBy="Select"
                            overrideStrings={internationalization}
                            filterOptions={filterOptions}
                        />
                    </div>
                )
            })}
            <PersonsWidget title='Актеры' storageName='favoriteActors'/>
            <PersonsWidget title='Режиссёры' storageName='favoriteDirectors'/>
        </div>
    )
}

export default Preferences
