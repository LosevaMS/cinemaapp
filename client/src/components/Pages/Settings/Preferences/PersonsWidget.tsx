import React, { useState } from 'react'

const PersonsWidget = ({ title, storageName }: any) => {

    const [currentActor, setCurrentActor] = useState('');
    const [actors, setActors] = useState(localStorage.getItem(storageName)?.length ?
        JSON.parse(localStorage.getItem(storageName)) : []);

    return (
        <div className='flex-preferences'>
            <p className='favorite-actors'>{title}</p>
            <div key='more-actors' className='more-criteria-div'>
                <div className='more-actors-div'>
                    <div className='more-actors-wrapper'>
                        {actors.map((person: any) => {
                            return (
                                <div className='more-actor-elem' key={person}>
                                    <p className='actor' >{person}</p>
                                    <button className='delete-actor-btn'
                                        onClick={() => {
                                            console.log('click')
                                            setActors(actors.filter((item: any) => item !== person));
                                            localStorage.setItem(storageName, JSON.stringify(actors.filter((item: any) => item !== person)));
                                        }}>
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            )
                        }
                        )}
                        <form onSubmit={(e) => {
                            e.preventDefault();
                            let newActors = actors;
                            newActors.push(currentActor);
                            setActors(newActors)
                            setCurrentActor('');
                            localStorage.setItem(storageName, JSON.stringify(newActors));
                            const actorElem = document.getElementById(storageName);
                            if (actorElem) actorElem.style.display = 'none';
                        }}>
                            <input type="text" id={storageName}
                                value={currentActor}
                                onChange={(e) => { setCurrentActor(e.target.value) }}
                                placeholder={title === 'Актеры' ? 'Актер' : 'Режиссёр'} />
                            <button className='hidden-btn' type="submit">Submit</button>
                        </form>
                        <button className='add-btn' onClick={() => {
                            const actorElem = document.getElementById(storageName);
                            if (actorElem)
                                if (window.getComputedStyle(actorElem, null).display == 'none') {
                                    actorElem.style.display = 'inline-block';
                                }
                        }}>+</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PersonsWidget
