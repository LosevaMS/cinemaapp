import React from 'react'
import Criteria from './Criteria/Criteria'
import Notification from './Notification/Notification'
import Profile from './Profile/Profile'
import Preferences from './Preferences/Preferences'
import './Settings.css'

function Settings() {
    return (
        <div>
            <div className='settings-flex'>
                <Profile />
                <Notification />
            </div>
            <Criteria />
            <Preferences />
        </div>
    )
}

export default Settings
