import React, { useState } from 'react'
import './Notification.css'
import { DragSwitch } from 'react-dragswitch'
import 'react-dragswitch/dist/index.css'

function Notification() {
    const [emailNotification, setEmailNotification] = useState(false)
    const [pushNotification, setPushNotification] = useState(false)

    return (
        <div className='notification-container'>
            <h3>Оповещения</h3>
            <DragSwitch
                onColor='#5750EC'
                offColor='#CDCDCD'
                focusShadow='0'
                checked={emailNotification}
                onChange={() => {
                    setEmailNotification(!emailNotification)
                }}
            />
            <span>Email-оповещения</span>
            <br></br>
            <DragSwitch
                onColor='#5750EC'
                offColor='#CDCDCD'
                focusShadow='0'
                checked={pushNotification}
                onChange={() => {
                    setPushNotification(!pushNotification)
                }}
            />
            <span>Push-уведомления</span>
        </div>
    )
}

export default Notification
