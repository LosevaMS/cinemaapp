import React, { useEffect, useState } from 'react'
import { observer, inject } from 'mobx-react'
import './Favorites.css'
import { IoIosArrowDown, IoIosArrowUp } from 'react-icons/io'
import MovieCard from '@app/components/MovieCard/MovieCard'
import { getRequest } from '@app/utils'
import { setFavorites } from '@app/store/UserStore/actions'
import axios from 'axios'
import { API_URL, AUTH_URL } from '@app/constants'

const date = {
    'января': 1,
    'февраля': 2,
    'марта': 3,
    'апреля': 4,
    'мая': 5,
    'июня': 6,
    'июля': 7,
    'августа': 8,
    'сентября': 9,
    'октября': 10,
    'ноября': 11,
    'декабря': 12
}

const Favorites = ({ rootStore }: any) => {

    const [dateFilter, setDateFilter] = useState({ decrease: true, isActive: false });
    const [ratingFilter, setRatingFilter] = useState({ decrease: true, isActive: true });
    const [user, setUser] = useState('');
    const [requestSended, setRequestSended] = useState(false);

    useEffect(() => {
        if (user === '')
            getRequest(AUTH_URL + '/current_user/',
                {
                    Authorization: `JWT ${localStorage.getItem('token')}`
                })
                .then(response => {
                    setUser(response.data.username);
                })

        if ((rootStore.userStore.favorites).length === 0  && !requestSended) {

            axios.get(API_URL + '/favorites/', {
                params: {
                    email: user
                }
            })
                .then(response => {
                    setFavorites(response.data.favorites)
                    setRequestSended(true);
                    console.log(response.data.favorites)
                })
                .catch(error => console.log(error))
        }
    })

    const onDateFilterClick = () => {
        setRatingFilter(prevState => {
            return { ...prevState, isActive: false }
        });
        if (dateFilter.decrease) setDateFilter({ decrease: false, isActive: true })
        if (!dateFilter.decrease) setDateFilter({ decrease: true, isActive: true })
    };
    const onRatingFilterClick = () => {
        setDateFilter(prevState => {
            return { ...prevState, isActive: false }
        });
        if (ratingFilter.decrease) setRatingFilter({ decrease: false, isActive: true })
        if (!ratingFilter.decrease) setRatingFilter({ decrease: true, isActive: true })
    }

    const onDelete = (Data: any) => {

        let del = confirm("Вы уверены, что хотите удалить фильм из избранного?");

        if (del == true) {

            axios.delete('http://localhost:8000/api/favorites/', {
                params: {
                    email: user,
                    data: Data
                }
            })
                .catch(error => console.log(error))

            const newList = (rootStore.userStore.favorites).filter((item: any) => item.name !== Data.name);
            setFavorites(newList);
        } else return;
    }

    const cardsRender = (ratingFilterIsActive: boolean) => {
        if (ratingFilterIsActive) {
            const myData = ([] as object[]).concat(rootStore.userStore.favorites)
                .sort((a, b) => ratingFilter.decrease ? (a.rating < b.rating ? 1 : -1) : (a.rating > b.rating ? 1 : -1))
                .map((item, i) =>
                    <MovieCard Data={item} key={item.name} isFavorites={true} onDelete={onDelete} parent={'favorites'} />
                );
            return myData;
        }
        else {
            const myData = ([] as object[]).concat(rootStore.userStore.favorites)
                .sort((a, b) => {
                    let newdate1 = (a.date).replace((a.date).split(" ")[1], date[(a.date).split(" ")[1]]);
                    let date1 = new Date(`${newdate1.split(" ")[1]}-${newdate1.split(" ")[0]}-${newdate1.split(" ")[2]}`);
                    if ((a.date).split(' ').length < 3) date1 = new Date('10-10-1600')
                    let newdate2 = (b.date).replace((b.date).split(" ")[1], date[(b.date).split(" ")[1]]);
                    let date2 = new Date(`${newdate2.split(" ")[1]}-${newdate2.split(" ")[0]}-${newdate2.split(" ")[2]}`);
                    if ((b.date).split(' ').length < 3) date2 = new Date(`10-11-1600`)
                    if (dateFilter.decrease)
                        if (date1 < date2) return 1;
                        else return -1;
                    else
                        if (date1 > date2) return 1;
                        else return -1;

                })
                .map((item, i) =>
                    <MovieCard Data={item} key={item.name} isFavorites={true} onDelete={onDelete} parent={'favorites'} />
                );
            return myData;
        }
    }

    return (
        <div className='favorites-container'>
            <div className='favorites-flex'>
                <h2 className='favorites-label'>Избранное</h2>
                <div className='filters-container'>
                    <span className={dateFilter.isActive ? 'filter-date' : 'filter'} onClick={onDateFilterClick} >Дата выхода
                    {dateFilter.decrease ? <IoIosArrowDown className='filter-arrow' />
                            : <IoIosArrowUp className='filter-arrow' />}</span>


                    <span className={ratingFilter.isActive ? 'filter-rating' : 'filter'} onClick={onRatingFilterClick}>Рейтинг
                    {ratingFilter.decrease ? <IoIosArrowDown className='filter-arrow' />
                            : <IoIosArrowUp className='filter-arrow' />}</span>

                </div>
            </div>
            <div className='favorites-cards'>
                {(rootStore.userStore.favorites).length !== 0 ? cardsRender(ratingFilter.isActive) : null}
            </div>
        </div>
    )
}

export default inject('rootStore')(observer(Favorites))
