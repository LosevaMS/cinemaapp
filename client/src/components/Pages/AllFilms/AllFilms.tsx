import React, { useEffect, useState } from 'react'
import { observer, inject } from 'mobx-react'
import { IoIosArrowDown, IoIosArrowUp } from 'react-icons/io'
import './AllFilms.css'
import MovieCard from '@app/components/MovieCard/MovieCard'
import { getRequest, postRequest } from '@app/utils'
import { MovieCardData } from '@app/store/Interfaces'
import { setAllFilms } from '@app/store/MoviesStore/actions'
import axios from 'axios'
import { API_URL, AUTH_URL } from '@app/constants'


const AllFilms = ({ rootStore }: any) => {

    const [filterDecrease, setFilterDecrease] = useState(true);
    const [user, setUser] = useState('');

    useEffect(() => {
        if (user === '')
            getRequest(AUTH_URL + '/current_user/',
                {
                    Authorization: `JWT ${localStorage.getItem('token')}`
                })
                .then(response => {
                    setUser(response.data.username);
                })

        if ((rootStore.moviesStore.allFilms).length === 0)
            axios.get(API_URL + '/allfilms/', {
                params: {
                    email: user
                }
            })
                .then(response => {
                    setAllFilms(response.data.allfilms)
                    console.log(response.data.allfilms)
                })
                .catch(error => console.log(error))
    })

    const onDelete = (Data: any) => {
        axios.delete(API_URL + '/favorites/', {
            params: {
                email: user,
                data: Data
            }
        })
            .catch(error => console.log(error))
    }

    const cardsRender = () => {
        const myData = ([] as object[]).concat(rootStore.moviesStore.allFilms)
            .sort((a, b) => filterDecrease ? (a.rating < b.rating ? 1 : -1) : (a.rating > b.rating ? 1 : -1))
            .map((item, i) =>
                <MovieCard Data={item} key={item.alias} parent={'allfilms'} onDelete={onDelete} user={user} />
            );
        return myData;
    }

    return (
        <div className='films-container'>
            <div className='films-flex'>
                <h2 className='films-label'>Сейчас в кино</h2>
                <div className='filters-container'>

                    <span className='filter' onClick={() => setFilterDecrease(!filterDecrease)}>Рейтинг
                    {filterDecrease ? <IoIosArrowDown className='filter-arrow' />
                            : <IoIosArrowUp className='filter-arrow' />}</span>

                </div>
            </div>

            <div className='films-cards'>
                {(rootStore.moviesStore.allFilms).length !== 0 ? cardsRender() : null}
            </div>
        </div>
    )
}

export default inject('rootStore')(observer(AllFilms))
