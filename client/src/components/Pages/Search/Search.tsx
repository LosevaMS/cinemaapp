import React, { useEffect } from 'react'
import { observer, inject } from 'mobx-react'
import './Search.css'
import SearchWidget from '@app/components/Pages/Search/SearchWidget/SearchWidget'
import Card from '@app/components/Pages/Search/Results/Card/Card'

const Search = ({ rootStore }: any) => {

    useEffect(() => {
        const searchContainer = document.getElementById('search-container');
        const cards = document.getElementsByClassName('card-list-wrapper');
        if (searchContainer && cards.length !== 0)
            searchContainer.style.background = 'none';
    })

    return (
        <div className='search-container' id='search-container'>
            <SearchWidget />
            <div className='results-wrapper'>
                <h2 className='results-label'>Результаты поиска</h2>
                {(rootStore.moviesStore.results).length === 0 ? (
                    <p className='no-result'>Нет результатов</p>
                ) : null}
                {((rootStore.moviesStore.results).length !== 0
                    && (rootStore.moviesStore.results).find((elem: any) => elem.status !== '')
                ) ? (
                    <div className={(rootStore.moviesStore.results).length > 1 ? 'card-list-wrapper-top' : 'card-list-wrapper'}>
                        {(rootStore.moviesStore.results).length === 0 ? ' ' :
                            (rootStore.moviesStore.results).map((item: any) => (
                                item.status !== '' ? <Card Data={item} key={item.alias} /> : ''
                            ))}
                    </div>)
                    : (null)}
                {(rootStore.moviesStore.results).length !== 0 ? (
                    <div className='card-list-wrapper'>
                        {(rootStore.moviesStore.results).length === 0 ? ' ' :
                            (rootStore.moviesStore.results).map((item: any) => (
                                item.status === '' ? <Card Data={item} key={item.alias} /> : ''
                            ))}
                    </div>)
                    : (null)}
            </div>
        </div>
    )
}

export default inject('rootStore')(observer(Search))
