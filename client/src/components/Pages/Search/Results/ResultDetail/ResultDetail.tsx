import React, { useEffect, useState } from 'react'
import './ResultDetail.css'
import { MdLocationOn } from 'react-icons/md'
import { observer, inject } from 'mobx-react'
import poster from '@assets/poster.png'


const ResultDetail = ({ rootStore }: any) => {

    const [film, setFilm] = useState({
        address: [],
        genre: [],
        age_limit: '',
        country: [],
        rating: '',
        movie: '',
        actors: [],
        image: '',
        sessions: [],
        directors: [],
        duration: ''
    });

    const criteriaItem = (criteria: string[]) => {
        return (
            <span className='info-span'>{criteria.map((item: string, index: number) => {
                if (index === criteria.length - 1) return (`${item}`)
                else return `${item}, `
            })}</span>
        )
    }

    useEffect(() => {
        const lastItem = (window.location.href).substring((window.location.href).lastIndexOf('/') + 1);
        (rootStore.moviesStore.results).forEach((item: any) => {
            if (item.alias === lastItem) setFilm(item);
        })
        console.log(film)
    })

    return (
        <div>
            <div className='movie-info-wrapper'>
                <img className='detail-poster' src={film.image ? film.image : poster} />
                <div className='movie-info'>
                    <h2 className='movie-name'>{film.movie}</h2>
                    <p className='info-country-age'>
                        {film.country ? criteriaItem(film.country) : null}</p>
                    <div className='detail-flex'>
                        <div className='detail-info-labels'>
                            {film.rating ? (<p className='detail-info'>Рейтинг:</p>) : ''}
                            {film.duration ? (<p className='detail-info'>Длительность:</p>) : ''}
                            {film.age_limit ? (<p className='detail-info'>Возрастное ограничение:</p>) : ''}
                            {film.genre ? (film.genre).length !== 0 ? (<p className='detail-info'>Жанр:</p>) : '' : ''}
                            {film.directors ? (film.directors).length !== 0 ? (<p className='detail-info'>{
                                (film.directors).length === 1 ? 'Режиссёр' : 'Режиссёры'
                            }:</p>) : '' : ''}
                            {film.actors ? (film.actors).length !== 0 ? (<p className='detail-info'>В ролях:</p>) : '' : ''}
                        </div>
                        <div className='detail-margin-left'>
                            <p className='info-span'>{film.rating}</p>
                            <p className='info-span'>{film.duration}</p>
                            <p className='info-span age-limit'>{film.age_limit}</p>
                            <p className='info-span'>{criteriaItem(film.genre)}</p>
                            <p className='info-span'>{criteriaItem(film.directors)}</p>
                            <p className='info-span'>{criteriaItem(film.actors)}</p>
                        </div>
                    </div>
                </div>
            </div>
            {film.sessions.map((session) => {
                return (
                    <div className='schedule-wrapper'>
                        <div className='cinema-info'>
                            <p className='cinema-name'>{session.cinema}</p>
                            <a className='cinema-address'
                                // href={session.href} 
                                target="_blank"><MdLocationOn />{session.address}</a>
                        </div>
                        <div className='schedule-cards'>
                            {session.schedule.map((schedule) => {
                                return (
                                    <div className='schedule-info' onClick={() => window.open(schedule.url)}>
                                        <span className='schedule-time'>{schedule.time}</span>
                                        <span className='schedule-price'>{schedule.price}₽</span>
                                        <span className='schedule-date'>{schedule.date}</span>
                                        <span className='schedule-format'>{schedule.format}</span>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default inject('rootStore')(observer(ResultDetail))
