import React from 'react'
import './Card.css'
import gold from '@assets/gold.svg'
import silver from '@assets/silver.svg'
import bronze from '@assets/bronze.svg'
import { Link, useHistory } from 'react-router-dom'
import poster from '@assets/poster.png'
import { GiCheckMark } from 'react-icons/gi'


function Card(props: any) {

    const criteriaItem = (criteria: string[], name: string) => {
        return (
            <p className='criteria'>{criteria.map((item: string, index: number) => {
                if (index === criteria.length - 1) return (`${item}`)
                else return `${item}, `
            })} {(props.Data.satisfied).includes(name) ? <GiCheckMark /> : ''}</p>
        )
    }
    const history = useHistory();

    return (
        <div className='card-wrapper'>
            <Link to={`search/${props.Data.alias}`}>
                <img className='poster' src={props.Data.image ? props.Data.image : poster} />
                <div className='info'>
                    <div className='criteria-labels'>
                        {props.Data.movie ? (<p className='movie'>
                            {props.Data.movie} {(props.Data.satisfied).includes('movie') ? <GiCheckMark /> : ''}
                        </p>) : null}
                        {(props.Data.genre && (props.Data.genre).length !== 0) ? criteriaItem(props.Data.genre, 'genre') : null}
                        {props.Data.age_limit ? (<p className='criteria'>
                            {props.Data.age_limit} {(props.Data.satisfied).includes('age_limit') ? <GiCheckMark /> : ''}
                        </p>) : null}
                        {(props.Data.country && (props.Data.country).length !== 0) ? criteriaItem(props.Data.country, 'country') : null}
                        {props.Data.rating ? (<p className='criteria'>
                            {props.Data.rating} {(props.Data.satisfied).includes('rating') ? <GiCheckMark /> : ''}
                        </p>) : null}
                        {(props.Data.directors && (props.Data.directors).length !== 0) ? criteriaItem(props.Data.directors, 'directors') : null}
                        {(props.Data.actors && (props.Data.actors).length !== 0) ? criteriaItem(props.Data.actors, 'actors') : null}
                    </div>
                    <div className='match-wrapper'>
                        <img className='award' src={props.Data.status === 'gold' ? gold :
                            props.Data.status === 'silver' ? silver :
                                props.Data.status === 'bronze' ? bronze : null} alt='' />
                        <span className='match'>Совпадений:<br /></span>
                        <span className='match-num'>{props.Data.matches}</span>
                    </div>
                </div>
            </Link>
            <div className='white-gradient'></div>
        </div>
    )
}

export default Card
