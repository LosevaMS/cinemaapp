import React, { useEffect } from 'react'
import TimeRangeSlider from 'react-time-range-slider'

const TimeSlider = ({ divClass, criteria, onClick, param, interval, classIsOpen, selectedValue, timeChangeHandler  }: any) => {

    return (
        <div key='time' className={divClass}>
            <p>{criteria}</p>
            <p className='placeholder-show slider' id='time-slider1'
                onClick={()=>{onClick(param)}}>{interval}</p>
            <div id='time-slider' className={classIsOpen}>
                <TimeRangeSlider
                    disabled={false}
                    format={24}
                    minValue={"08:00"}
                    value={selectedValue}
                    maxValue={"23:59"}
                    name={"time_range"}
                    onChange={timeChangeHandler}
                    step={15} />
            </div>
        </div>
    )
}

export default TimeSlider
