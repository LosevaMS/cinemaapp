import React, { useEffect } from 'react'
import Slider from '@material-ui/core/Slider'
import { withStyles } from '@material-ui/core/styles'

const CSlider = withStyles({
    root: {
        color: '#5750EC',
        height: 4,
    },
    thumb: {
        height: 14,
        width: 14,
        backgroundColor: '#fff',
        border: '0',
        boxShadow: '0px -1px 5px rgb(0 0 0 / 2%), 0px 1px 5px rgb(0 0 0 / 20%)',
        '&:focus, &:hover, &active': {
            boxShadow: '0px -1px 5px rgb(0 0 0 / 2%), 0px 1px 5px rgb(0 0 0 / 20%)',
        },
    },
    track: {
        height: 4,
        borderRadius: 4,
    },
    rail: {
        height: 4,
        borderRadius: 4
    },
})(Slider);


const CustomSlider = ({ divKey, criteriaDivClass, criteria, pId, pOnClick, onClickParam, interval,
    sliderDivId, sliderClass, minVal, maxVal, stepVal, selectedVal, handleChange }: any) => {
    
    return (
        <div key={divKey} className={criteriaDivClass}>
            <p>{criteria}</p>
            <p className='placeholder-show slider' id={pId}
                onClick={() => { pOnClick(onClickParam) }}>{interval}</p>
            <div id={sliderDivId} className={sliderClass}>
                <CSlider
                    min={minVal}
                    max={maxVal}
                    step={stepVal}
                    value={selectedVal}
                    onChange={handleChange}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                />
            </div>
        </div>
    )
}

export default CustomSlider
