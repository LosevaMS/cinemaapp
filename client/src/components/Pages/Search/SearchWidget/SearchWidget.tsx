import React, { useState, useEffect } from 'react'
import './SearchWidget.css'
import { IoIosSearch } from 'react-icons/io'
import { BsFilterLeft } from 'react-icons/bs'
import { observer, inject } from 'mobx-react'
import { CriteriaData } from '@app/components/Pages/Search/SearchWidget/CriteriaData'
import MultiSelect from "react-multi-select-component"
import TimeRangeSlider from 'react-time-range-slider'
import axios from 'axios'
import { runInAction } from 'mobx'
import CustomSlider from './Slider/CustomSlider'
import TimeSlider from './TimeSlider/TimeSlider'
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css";
import { setCriteria, setResults } from '@app/store/MoviesStore/actions'
import { API_URL } from '@app/constants'



const internationalization = {
    "allItemsAreSelected": "Выбрано все",
    "clearSearch": "Очистить поиск",
    "noOptions": "Не найдено",
    "search": "Поиск",
    "selectAll": "Выбрать все",
    "selectSomeItems": "Выбрать..."
}

const SearchWidget = ({ rootStore }: any) => {
    const [currentActor, setCurrentActor] = useState('');
    const [isActorsOpen, setIsActorsOpen] = useState(false);
    const [isCounterOpen, setIsCounterOpen] = useState(false);
    const [isRatingOpen, setIsRatingOpen] = useState(false);
    const [isTimeOpen, setIsTimeOpen] = useState(false);
    const [isPriceOpen, setIsPriceOpen] = useState(false);
    const [isMoreOpen, setIsMoreOpen] = useState(false);
    const [startDate, setStartDate] = useState(new Date());
    const [selected, setSelected] = useState(rootStore.moviesStore.criteria);

    useEffect(() => {
        if (isMoreOpen) {
            window.addEventListener('click', function (e) {
                const moreElem = document.getElementById('more');
                const more1Elem = document.getElementById('more1');
                if (moreElem && more1Elem && !moreElem.contains(e.target as Node) && !more1Elem.contains(e.target as Node)) {
                    setIsMoreOpen(!isMoreOpen);
                }
                if (moreElem && moreElem.contains(e.target as Node)) setIsMoreOpen(true);
            });
        }
        if (isPriceOpen) {
            window.addEventListener('click', function (e) {
                const priceElem = document.getElementById('price-slider');
                const price1Elem = document.getElementById('price-slider1');
                if (priceElem && price1Elem && !priceElem.contains(e.target as Node) && !price1Elem.contains(e.target as Node)) {
                    setIsPriceOpen(!isPriceOpen)
                }
            });
            window.addEventListener('keydown', function (e) {
                if (e.key === 'Enter' && isPriceOpen) {
                    setIsPriceOpen(!isPriceOpen)
                }
            });
        }
        if (isTimeOpen) {
            window.addEventListener('click', function (e) {
                const timeElem = document.getElementById('time-slider');
                const time1Elem = document.getElementById('time-slider1');
                if (timeElem && time1Elem && !timeElem.contains(e.target as Node) && !time1Elem.contains(e.target as Node)) {
                    setIsTimeOpen(!isTimeOpen)
                }
            });
            window.addEventListener('keydown', function (e) {
                if (e.key === 'Enter' && isTimeOpen) {
                    setIsTimeOpen(!isTimeOpen)
                }
            });
        }
        if (isRatingOpen) {
            window.addEventListener('click', function (e) {
                const ratingElem = document.getElementById('rating-slider');
                const rating1Elem = document.getElementById('rating-slider1');
                if (ratingElem && rating1Elem && !ratingElem.contains(e.target as Node) && !rating1Elem.contains(e.target as Node)) {
                    setIsRatingOpen(!isRatingOpen)
                }
            });
            window.addEventListener('keydown', function (e) {
                if (e.key === 'Enter' && isRatingOpen) {
                    setIsRatingOpen(!isRatingOpen)
                }
            });
        }
        if (isCounterOpen) {
            window.addEventListener('click', function (e) {
                const counterElem = document.getElementById('counter');
                const counter1Elem = document.getElementById('counter1');
                if (counterElem && counter1Elem && !counterElem.contains(e.target as Node) && !counter1Elem.contains(e.target as Node)) {
                    setIsCounterOpen(!isCounterOpen)
                }
            });
            window.addEventListener('keydown', function (e) {
                if (e.key === 'Enter' && isCounterOpen) {
                    setIsCounterOpen(!isCounterOpen)
                }
            });
        }
        if (isActorsOpen) {
            window.addEventListener('click', function (e) {
                const actorsElem = document.getElementById('actors');
                const actors1Elem = document.getElementById('actors1');
                if (actorsElem && actors1Elem && !actorsElem.contains(e.target as Node) && !actors1Elem.contains(e.target as Node)) {
                    setIsActorsOpen(!isActorsOpen)
                }
            });

        }
    })

    const handleKeyPress = (event: any) => {
        if (event.key === 'Enter' && isCounterOpen) {
            setIsCounterOpen(!isCounterOpen)
        }
    }

    function filterOptions(options: any, filter: any) {
        if (!filter) {
            return options;
        }
        const re = new RegExp(filter, "i");
        return options.filter(({ value }: any) => value && value.match(re));
    }

    const onSelectChanged = (e: any, alias: String) => {
        const value = e;
        switch (alias) {
            case 'address':
                setSelected(prevState => {
                    return { ...prevState, address: value }
                });
                break;
            case 'genre':
                setSelected(prevState => {
                    return { ...prevState, genre: value }
                });
                break;
            case 'age_limit':
                setSelected(prevState => {
                    return { ...prevState, age_limit: value }
                });
                break;
            case 'country':
                setSelected(prevState => {
                    return { ...prevState, country: value }
                });
                break;
            case 'row':
                setSelected(prevState => {
                    return { ...prevState, row: value }
                });
                break;
            case 'format':
                setSelected(prevState => {
                    return { ...prevState, format: value }
                });
                break;
            case 'ticket_number':
                setSelected(prevState => {
                    return { ...prevState, ticket_number: value }
                });
                break;
            case 'session_time':
                setSelected(prevState => {
                    return { ...prevState, session_time: value }
                });
                break;
            case 'rating':
                setSelected(prevState => {
                    return { ...prevState, rating: value }
                });
                break;
            case 'price':
                setSelected(prevState => {
                    return { ...prevState, price: value }
                });
                break;
            case 'movie':
                setSelected(prevState => {
                    return { ...prevState, movie: value.target.value }
                });
                break;
            case 'actors':
                setSelected(prevState => {
                    return { ...prevState, actors: value }
                });
                break;
        }
    }

    const handleIncrease = () => {
        setSelected(prevState => {
            let value = prevState.ticket_number;
            if (value < 10) value++;
            return { ...prevState, ticket_number: value }
        });
    }
    const handleDecrease = () => {
        setSelected(prevState => {
            let value = prevState.ticket_number;
            if (value > 0) value--;
            return { ...prevState, ticket_number: value }
        });
    }

    const handleRatingChange = (event: any, newValue: number[]) => {
        let rating = newValue;
        setSelected({ ...selected, rating })
    }

    const timeChangeHandler = (newTime: any) => {
        let time = newTime;
        setSelected({ ...selected, time })
    }

    const handlePriceChange = (event: any, newValue: number) => {
        let price = newValue;
        setSelected({ ...selected, price })
    };

    let counter = 0;

    return (
        <div className='search-widget-container'>

            {CriteriaData.map((criteria) => {
                let value = [];
                switch (criteria.alias) {
                    case 'address':
                        value = selected.address;
                        break;
                    case 'genre':
                        value = selected.genre;
                        break;
                    case 'age_limit':
                        value = selected.age_limit;
                        break;
                    case 'country':
                        value = selected.country;
                        break;
                    case 'row':
                        value = selected.row;
                        break;
                    case 'format':
                        value = selected.format;
                        break;
                    case 'ticket_number':
                        value = selected.ticket_number;
                        break;
                    case 'session_time':
                        value = selected.time;
                        break;
                    case 'rating':
                        value = selected.rating;
                        break;
                    case 'price':
                        value = selected.price;
                        break;
                    case 'movie':
                        value = selected.movie;
                        break;
                    case 'actors':
                        value = selected.actors;
                        break;
                }
                if (localStorage.getItem('Criteria')?.includes(criteria.criteria)) {
                    counter++;
                    if (criteria.view === 'select') return (
                        <div key={`select${counter}`}
                            className={counter == 1 ? 'criteria-div' : 'criteria-div border-before'}>
                            <p>{criteria.criteria}</p>
                            <p className={value.length > 0 ? 'placeholder-hidden' : 'placeholder-show'}>{criteria.placeholder}</p>
                            <MultiSelect
                                options={criteria.options}
                                value={value}
                                onChange={(e: any) => { onSelectChanged(e, criteria.alias) }}
                                labelledBy="Select"
                                overrideStrings={internationalization}
                                filterOptions={filterOptions}
                            />
                        </div >)
                    if (criteria.view === 'slider' && criteria.alias !== 'session_time') return (
                        <CustomSlider divKey={criteria.alias === 'rating' ? 'rating' : 'price'}
                            criteriaDivClass={counter == 1 ? 'criteria-div' : 'criteria-div border-before'}
                            criteria={criteria.criteria} pId={criteria.alias === 'rating' ? 'rating-slider1' : 'price-slider1'}
                            pOnClick={criteria.alias === 'rating' ? setIsRatingOpen : setIsPriceOpen}
                            onClickParam={criteria.alias === 'rating' ? !isRatingOpen : !isPriceOpen}
                            interval={criteria.alias === 'rating' ? `от ${selected.rating[0]} до ${selected.rating[1]}`
                                : `до ${selected.price} руб.`}
                            sliderDivId={criteria.alias === 'rating' ? 'rating-slider' : 'price-slider'}
                            sliderClass={criteria.alias === 'rating' ? isRatingOpen ? 'slider-div' : 'div-hidden'
                                : isPriceOpen ? 'slider-div' : 'div-hidden'}
                            minVal={0} maxVal={criteria.alias === 'rating' ? 10 : 1000}
                            stepVal={criteria.alias === 'rating' ? 0.5 : 50}
                            selectedVal={criteria.alias === 'rating' ? selected.rating : selected.price}
                            handleChange={criteria.alias === 'rating' ? handleRatingChange : handlePriceChange} />
                    )
                    if (criteria.view === 'slider' && criteria.alias == 'session_time') return (
                        <TimeSlider
                            divClass={counter == 1 ? 'criteria-div' : 'criteria-div border-before'}
                            criteria={criteria.criteria} onClick={setIsTimeOpen} param={!isTimeOpen}
                            interval={`${selected.time.start}-${selected.time.end}`} classIsOpen={isTimeOpen ? 'slider-div' : 'div-hidden'}
                            selectedValue={selected.time} timeChangeHandler={timeChangeHandler} />
                    )
                    if (criteria.view === 'counter') return (
                        <div key='ticket-num' className={counter == 1 ? 'criteria-div' : 'criteria-div border-before'}>
                            <p>{criteria.criteria}</p>
                            <p className='placeholder-show' id='counter1'
                                onClick={() => { setIsCounterOpen(!isCounterOpen) }}>{selected.ticket_number}</p>
                            <div id='counter' className={isCounterOpen ? 'counter-div' : 'div-hidden'}>
                                <div className={selected.ticket_number == 0 ? 'counter-btn disabled' : 'counter-btn'}
                                    onClick={handleDecrease}>-</div>
                                <span>{selected.ticket_number}</span>
                                <div className={selected.ticket_number > 9 ? 'counter-btn disabled' : 'counter-btn'}
                                    onClick={handleIncrease}>+</div>
                            </div>
                        </div>
                    )
                    if (criteria.view === 'input') return (
                        <div key='movie' className={counter == 1 ? 'criteria-div' : 'criteria-div border-before'}>
                            <p>{criteria.criteria}</p>
                            <input type="text"
                                value={value}
                                onChange={(e) => onSelectChanged(e, criteria.alias)}
                                placeholder={criteria.placeholder} />
                        </div>
                    )
                    if (criteria.view === 'custom_input') return (
                        <div key='actors' className={counter == 1 ? 'criteria-div' : 'criteria-div border-before'}>
                            <p>{criteria.criteria}</p>
                            <p className='actors-placeholder' id='actors1'
                                onClick={() => { setIsActorsOpen(!isActorsOpen) }}>
                                {selected.actors.length === 0 ? criteria.placeholder :
                                    selected.actors.map((actor, index) => {
                                        if (index === selected.actors.length - 1) return (`${actor}`)
                                        else return `${actor}, `
                                    })}</p>
                            <div id='actors' className={isActorsOpen ? 'actors-div' : 'div-hidden'}>
                                <div className='actors-wrapper'>
                                    {selected.actors.map((actor, index) => {
                                        return (
                                            <div>
                                                <p className='actor'>{actor}</p>
                                                <button className='delete-actor-btn'
                                                    onClick={() => {
                                                        setIsActorsOpen(true);
                                                        const actors = selected.actors;
                                                        actors.splice(index, 1);
                                                        setSelected({ ...selected, actors })
                                                    }}>
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                        )
                                    }
                                    )}
                                    <form onSubmit={(e) => {
                                        e.preventDefault();
                                        let actors = selected.actors;
                                        actors.push(currentActor);
                                        setSelected({ ...selected, actors })
                                        setCurrentActor('');
                                        const actorElem = document.getElementById('actor-input');
                                        if (actorElem) actorElem.style.display = 'none';
                                    }}>
                                        <input type="text" id='actor-input'
                                            value={currentActor}
                                            onChange={(e) => { setCurrentActor(e.target.value) }}
                                            placeholder='Актер' />
                                        <button className='hidden-btn' type="submit">Submit</button>
                                    </form>
                                    <button className='add-btn' onClick={() => {
                                        const actorElem = document.getElementById('actor-input');
                                        if (actorElem)
                                            if (window.getComputedStyle(actorElem, null).display == 'none') {
                                                actorElem.style.display = 'inline-block';
                                            }
                                    }}>+</button>
                                </div>
                            </div>
                        </div>
                    )
                }
            })}

            <div id='more1'
                className='criteria-div border-before'
                onClick={() => { setIsMoreOpen(!isMoreOpen) }}>
                <p>Ещё</p>
                <BsFilterLeft className='more-icon' />
                <div id='more' className={isMoreOpen ? 'more-div' : 'div-hidden'}
                >
                    {CriteriaData.map((criteria) => {
                        let value = [];
                        switch (criteria.alias) {
                            case 'address':
                                value = selected.address;
                                break;
                            case 'genre':
                                value = selected.genre;
                                break;
                            case 'age_limit':
                                value = selected.age_limit;
                                break;
                            case 'country':
                                value = selected.country;
                                break;
                            case 'row':
                                value = selected.row;
                                break;
                            case 'format':
                                value = selected.format;
                                break;
                            case 'ticket_number':
                                value = selected.ticket_number;
                                break;
                            case 'session_time':
                                value = selected.time;
                                break;
                            case 'rating':
                                value = selected.rating;
                                break;
                            case 'price':
                                value = selected.price;
                                break;
                            case 'movie':
                                value = selected.movie;
                                break;
                            case 'actors':
                                value = selected.actors;
                                break;
                        }
                        if (criteria.isDefault === false) {
                            if (criteria.view === 'input') return (
                                <div key='more-movie' className='more-criteria-div'>
                                    <p>{criteria.criteria}</p>
                                    <input type="text"
                                        value={value}
                                        onChange={(e) => onSelectChanged(e, criteria.alias)}
                                        placeholder={criteria.placeholder} />
                                </div>
                            )
                            if (criteria.view === 'select') return (
                                <div key={`more-select${criteria.alias}`} className='more-criteria-div'>
                                    <p>{criteria.criteria}</p>
                                    <MultiSelect
                                        options={criteria.options}
                                        value={value}
                                        onChange={(e) => { onSelectChanged(e, criteria.alias) }}
                                        labelledBy="Select"
                                        overrideStrings={internationalization}
                                        filterOptions={filterOptions}
                                    />
                                </div>
                            )
                            if (criteria.view === 'datepicker') return (
                                <div key='more-datepicker' className='more-criteria-div'>
                                    <p>{criteria.criteria}</p>
                                    <DatePicker selected={startDate}
                                        dateFormat="dd-MM-yyyy"
                                        onChange={date => {
                                            setStartDate(date);
                                            let now = new Date(date);
                                            let month = String(now.getMonth()).length === 1 ? `0${now.getMonth() + 1}` : `${now.getMonth() + 1}`;
                                            let value = `${now.getDate()}-${month}-${now.getFullYear()}`;
                                            setSelected(prevState => {
                                                return { ...prevState, date: value }
                                            });
                                        }} />
                                </div>
                            )
                            if (criteria.view === 'counter') return (
                                <div key='more-ticket-num' className='more-criteria-div'>
                                    <p>{criteria.criteria}</p>
                                    <div className='more-counter-div'>
                                        <div className={selected.ticket_number == 0 ? 'counter-btn disabled' : 'counter-btn'}
                                            onClick={handleDecrease}>-</div>
                                        <span>{selected.ticket_number}</span>
                                        <div className={selected.ticket_number > 9 ? 'counter-btn disabled' : 'counter-btn'}
                                            onClick={handleIncrease}>+</div>
                                    </div>
                                </div>
                            )
                            if (criteria.view === 'slider' && criteria.alias !== 'session_time') return (
                                <CustomSlider divKey={criteria.alias === 'rating' ? 'more-rating' : 'more-price'}
                                    criteriaDivClass='more-criteria-div'
                                    criteria={criteria.criteria} pId=''
                                    pOnClick={null}
                                    onClickParam={null}
                                    interval={criteria.alias === 'rating' ? `от ${selected.rating[0]} до ${selected.rating[1]}`
                                        : `до ${selected.price} руб.`}
                                    sliderDivId=''
                                    sliderClass='more-slider-div'
                                    minVal={0} maxVal={criteria.alias === 'rating' ? 10 : 1000}
                                    stepVal={criteria.alias === 'rating' ? 0.5 : 50}
                                    selectedVal={criteria.alias === 'rating' ? selected.rating : selected.price}
                                    handleChange={criteria.alias === 'rating' ? handleRatingChange : handlePriceChange} />
                            )
                            if (criteria.view === 'slider' && criteria.alias == 'session_time') return (
                                <div key='more-time' className='more-criteria-div'>
                                    <p>{criteria.criteria}</p>
                                    <p className='placeholder-show slider'>{`${selected.time.start}-${selected.time.end}`}</p>
                                    <div className='more-slider-div'>
                                        <TimeRangeSlider
                                            disabled={false}
                                            format={24}
                                            maxValue={"23:59"}
                                            value={selected.time}
                                            minValue={"08:00"}
                                            name={"time_range"}
                                            onChange={timeChangeHandler}
                                            step={15} />
                                    </div>
                                </div>
                            )
                            if (criteria.view === 'custom_input') return (
                                <div key='more-actors' className='more-criteria-div'>
                                    <p>{criteria.criteria}</p>
                                    <div className='more-actors-div'>
                                        <div className='more-actors-wrapper'>
                                            {selected.actors.map((actor, index) => {
                                                return (
                                                    <div className='more-actor-elem'>
                                                        <p className='actor'>{actor}</p>
                                                        <button className='delete-actor-btn'
                                                            onClick={() => {
                                                                setIsActorsOpen(true);
                                                                const actors = selected.actors;
                                                                actors.splice(index, 1);
                                                                setSelected({ ...selected, actors })
                                                            }}>
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                )
                                            }
                                            )}
                                            <form onSubmit={(e) => {
                                                e.preventDefault();
                                                let actors = selected.actors;
                                                actors.push(currentActor);
                                                setSelected({ ...selected, actors })
                                                setCurrentActor('');
                                                const actorElem = document.getElementById('actor-input');
                                                if (actorElem) actorElem.style.display = 'none';
                                            }}>
                                                <input type="text" id='actor-input'
                                                    value={currentActor}
                                                    onChange={(e) => { setCurrentActor(e.target.value) }}
                                                    placeholder='Актер' />
                                                <button className='hidden-btn' type="submit">Submit</button>
                                            </form>
                                            <button className='add-btn' onClick={() => {
                                                const actorElem = document.getElementById('actor-input');
                                                if (actorElem)
                                                    if (window.getComputedStyle(actorElem, null).display == 'none') {
                                                        actorElem.style.display = 'inline-block';
                                                    }
                                            }}>+</button>
                                        </div>

                                    </div>
                                </div>
                            )
                        }
                    })}
                </div>
            </div>
            <div className='search-btn' onClick={() => {
                setCriteria(selected);

                axios.get(API_URL + '/results/', { params: { data: selected } })
                    .then(response => {
                        console.log(response.data.results);
                        setResults(response.data.results);
                    })
                    .catch(error => console.log(error))
            }}>
                <IoIosSearch />Поиск
            </div>
        </div >
    )
}

export default inject('rootStore')(observer(SearchWidget));