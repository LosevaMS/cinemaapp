import React, { useEffect, useState } from 'react'
import { observer, inject } from 'mobx-react'
import { IoIosArrowDown, IoIosArrowUp } from 'react-icons/io'
import './Recommendations.css'
import MovieCard from '@app/components/MovieCard/MovieCard'
import { MovieCardData } from '@app/store/Interfaces'
import { getRequest } from '@app/utils'
import { setRecommendations } from '@app/store/UserStore/actions'
import axios from 'axios'
import { API_URL, AUTH_URL } from '@app/constants'

const date = {
    'января': 1,
    'февраля': 2,
    'марта': 3,
    'апреля': 4,
    'мая': 5,
    'июня': 6,
    'июля': 7,
    'августа': 8,
    'сентября': 9,
    'октября': 10,
    'ноября': 11,
    'декабря': 12
}

const Recommendations = ({ rootStore }: any) => {

    const [filterDecrease, setFilterDecrease] = useState(true);
    const [user, setUser] = useState('');
    const [requestSended, setRequestSended] = useState(false);

    useEffect(() => {
        if (user === '')
            getRequest(AUTH_URL + '/current_user/',
                {
                    Authorization: `JWT ${localStorage.getItem('token')}`
                })
                .then(response => {
                    setUser(response.data.username);
                })

        let isStorageEmpty = localStorage.getItem('favoriteGenres') === null && localStorage.getItem('favoriteCountries') === null
            && localStorage.getItem('favoriteActors') === null && localStorage.getItem('favoriteAgeLimits') === null
            && localStorage.getItem('favoriteDirectors') === null;
        console.log(isStorageEmpty)

        if ((rootStore.userStore.recommendations).length === 0 && !isStorageEmpty && !requestSended && user !== '')

            axios.get(API_URL + '/recommendations/', {
                params: {
                    email: user,
                    preferences: {
                        genres: localStorage.getItem('favoriteGenres') === null ? [] : JSON.parse(String(localStorage.getItem('favoriteGenres'))),
                        countries: localStorage.getItem('favoriteCountries') === null ? [] : JSON.parse(String(localStorage.getItem('favoriteCountries'))),
                        actors: localStorage.getItem('favoriteActors') === null ? [] : JSON.parse(String(localStorage.getItem('favoriteActors'))),
                        age_limit: localStorage.getItem('favoriteAgeLimits') === null ? [] : JSON.parse(String(localStorage.getItem('favoriteAgeLimits'))),
                        directors: localStorage.getItem('favoriteDirectors') === null ? [] : JSON.parse(String(localStorage.getItem('favoriteDirectors')))
                    }
                }
            })
                .then(response => {
                    setRecommendations(response.data.recommendations)
                    setRequestSended(true);
                    console.log(response.data.recommendations)
                })
                .catch(error => console.log(error))
    })


    const onDelete = (Data: any) => {
        axios.delete(API_URL + '/favorites/', {
            params: {
                email: user,
                data: Data
            }
        })
            .catch(error => console.log(error))
    }

    const cardsRender = () => {
        const myData = ([] as object[]).concat(rootStore.userStore.recommendations)
            .sort((a, b) => {
                let newdate1 = (a.date).replace((a.date).split(" ")[1], date[(a.date).split(" ")[1]]);
                let date1 = new Date(`${newdate1.split(" ")[1]}-${newdate1.split(" ")[0]}-${newdate1.split(" ")[2]}`);
                let newdate2 = (b.date).replace((b.date).split(" ")[1], date[(b.date).split(" ")[1]]);
                let date2 = new Date(`${newdate2.split(" ")[1]}-${newdate2.split(" ")[0]}-${newdate2.split(" ")[2]}`);
                if (filterDecrease)
                    if (date1 < date2) return 1;
                    else return -1;
                else
                    if (date1 > date2) return 1;
                    else return -1;
            })
            .map((item, i) =>
                <MovieCard Data={item} key={item.alias} parent={'recommendations'} onDelete={onDelete} user={user} />
            );
        return myData;
    }

    return (
        <div className='recommendations-container'>
            <div className='recommendations-flex'>
                <h2 className='recommendations-label'>Рекомендации</h2>
                <div className='filters-container'>
                    <span className='filter' onClick={() => setFilterDecrease(!filterDecrease)} >Дата выхода
                    {filterDecrease ? <IoIosArrowDown className='filter-arrow' />
                            : <IoIosArrowUp className='filter-arrow' />}</span>

                </div>
            </div>
            {(rootStore.userStore.recommendations).length === 0 && user !== ''  && requestSended ? (
                <p className='no-recom-warning'>На данный момент рекомендации отсутствуют. Пожалуйста, настройте предпочтения или
                    добавьте понравившиеся фильмы из раздела "Сейчас в кино" в "Избранное"</p>
            ) : null}
            <div className='recommendations-cards'>
                {(rootStore.userStore.recommendations).length !== 0 ? cardsRender() : null}
            </div>
        </div>
    )
}

export default inject('rootStore')(observer(Recommendations))
