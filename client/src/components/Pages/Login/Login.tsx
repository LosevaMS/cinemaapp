import React, { useEffect, useState } from 'react';
import './Login.css';
import { FormControl } from 'react-bootstrap'
import GoogleLogin from 'react-google-login'
import { observer, inject } from 'mobx-react'
import { doLogin, signUp, doGoogleLogin } from '@app/store/UserStore/actions'
import { useHistory } from "react-router"
import { clientId } from '@app/constants'


const Login = ({ rootStore }: any) => {
    const history = useHistory();

    const [isAccount, setIsAccount] = useState(true);

    const [state, setState] = useState({
        username: '',
        first_name: '',
        password: ''
    })

    useEffect(() => {
        console.log(localStorage.getItem('Criteria'));
        if (!rootStore.userStore.loggedIn && localStorage.getItem('token')) {
            rootStore.userStore.loggedIn = true;
        }
        if (rootStore.userStore.loggedIn) {
            if (localStorage.getItem('Criteria') === null) {
                localStorage.setItem('Criteria', JSON.stringify(['Адрес кинотеатра', 'Жанр', 'Рейтинг']));
                history.push('/search')
            }
        }
    })

    const onAuthSubmit = (e: any, state: any, isAccount: boolean) => {
        if (isAccount)
            doLogin(e, state);
        else signUp(e, state)

        setInterval(function () {
            if (rootStore.userStore.loggedIn) {
                localStorage.setItem('Criteria', JSON.stringify(['Адрес кинотеатра', 'Жанр', 'Рейтинг']));
                history.push('/search')
            }
        }, 300)
    }

    const handleChange = (e: any) => {
        const name = e.target.name;
        if (name === 'username') {
            const username = e.target.value;
            setState({ ...state, username });
        }
        if (name === 'first_name') {
            const first_name = e.target.value;
            setState({ ...state, first_name });
        }
        if (name === 'password') {
            const password = e.target.value;
            setState({ ...state, password });
        }
    };

    const googleOnSuccess = (response: any) => {
        doGoogleLogin(response.tokenId);
        setInterval(function () {
            if (rootStore.userStore.loggedIn) {
                localStorage.setItem('Criteria', JSON.stringify(['Адрес кинотеатра', 'Жанр', 'Рейтинг']));
                history.push('/search')
            }
        }, 300)
    }
    const googleOnFailure = (response: any) => {
        console.log('[Login failed] response: ', response);
    }

    return (
        <div className='container'>
            {rootStore.userStore.loggedIn ? null : (
                <div className={isAccount ? 'authWindow signIn' : 'authWindow'}>
                    <div className={isAccount ? 'formContainer signInContainer' : 'formContainer'}>
                        <form onSubmit={(e) => onAuthSubmit(e, state, isAccount)}>
                            <h1>logo</h1>
                            <h3>{isAccount ? 'Вход' : 'Регистрация'}</h3>
                            {isAccount ? null : (
                                <FormControl type="text" placeholder="Имя"
                                    name='first_name'
                                    value={state.first_name}
                                    onChange={handleChange} />
                            )}
                            <FormControl type="email" placeholder="E-mail"
                                name='username'
                                value={state.username}
                                onChange={handleChange} />
                            <FormControl type="password" placeholder="Пароль"
                                name='password'
                                value={state.password}
                                onChange={handleChange} />
                            {isAccount ? null : (<FormControl type="password" placeholder="Подтвердите пароль" />)}
                            <button className="signUpBtn" type='submit'>{isAccount ? 'Войти' : 'Зарегистрироваться'}</button>

                            <GoogleLogin
                                clientId={clientId}
                                render={renderProps => (
                                    <button onClick={renderProps.onClick}
                                        disabled={renderProps.disabled}
                                        className="signUpWithGoogleBtn">Вход через Google</button>
                                )}
                                onSuccess={googleOnSuccess}
                                onFailure={googleOnFailure}
                                cookiePolicy={'single_host_origin'}
                                isSignedIn={false}
                            />

                            <span>{isAccount ? 'Нет аккаунта?' : 'Уже есть аккаунт?'}</span>
                            <a className="blue" href="#"
                                onClick={() => setIsAccount(!isAccount)}>{isAccount ? ' Регистрация' : ' Войти'}</a>
                        </form>
                    </div>
                </div>
            )}
        </div>
    );
};
export default inject('rootStore')(observer(Login));