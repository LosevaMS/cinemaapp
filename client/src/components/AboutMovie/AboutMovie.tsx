import React, { useEffect, useState } from 'react'
import './AboutMovie.scss'
import { observer, inject } from 'mobx-react'
import poster from '@assets/poster.png'


const AboutMovie = ({ rootStore }: any) => {

    const [film, setFilm] = useState({
        genres: [],
        age_limit: '',
        countries: [],
        rating: '',
        name: '',
        actors: [],
        image: '',
        date: '',
        description: [],
        directors: [],
        video: ''
    });

    const criteriaItem = (criteria: string[]) => {
        return (
            <span className='info-span'>{criteria.map((item: string, index: number) => {
                if (index === criteria.length - 1) return (`${item}`)
                else return `${item}, `
            })}</span>
        )
    }

    useEffect(() => {

        let ret = (window.location.href).replace((window.location.href).substring((window.location.href).lastIndexOf('/') + 1), '');
        ret = ret.substring(0, ret.length - 1);
        let previous = ret.substring(ret.lastIndexOf('/') + 1);
        const lastItem = (window.location.href).substring((window.location.href).lastIndexOf('/') + 1);

        if (previous === 'allfilms') {
            (rootStore.moviesStore.allFilms).forEach((item: any) => {
                if (item.alias === lastItem) setFilm(item);
            })
            console.log(film)
        }
        if (previous === 'favorites') {
            (rootStore.userStore.favorites).forEach((item: any) => {
                if (item.alias === lastItem) setFilm(item);
            })
            console.log(film)
        }
        if (previous === 'recommendations') {
            (rootStore.userStore.recommendations).forEach((item: any) => {
                if (item.alias === lastItem) setFilm(item);
            })
            console.log(film)
        }

    })

    return (
        <div className='movie-info-wrapper'>
            <div>
                <img className='about-poster' src={film.image ? film.image : poster} />
                {film.video.length > 1 ? (<div className="wrapper">
                    <input type="checkbox" />
                    <div className="video">
                        <video src={film.video} loop autoPlay></video>
                    </div>
                    <div className="text">
                        <span data-text="Смотреть превью"></span>
                    </div>
                </div>) : (null)}
            </div>
            <div className='movie-info'>
                <h2 className='movie-name'>{film.name}</h2>
                <p className='info-country-age'>
                    {criteriaItem(film.countries)}</p>
                <div className='detail-flex'>
                    <div className='detail-info-labels'>
                        {film.date ? (<p className='detail-info date'>Дата выхода:</p>) : ''}
                        {film.rating ? (<p className='detail-info'>Рейтинг:</p>) : ''}
                        {film.age_limit ? (<p className='detail-info'>Возрастное ограничение:</p>) : ''}
                        {(film.genres).length !== 0 ? (<p className='detail-info'>Жанр:</p>) : ''}
                        {(film.directors).length !== 0 ? (<p className='detail-info'>Режиссёр:</p>) : ''}
                        {(film.actors).length !== 0 ? (<p className='detail-info'>В ролях:</p>) : ''}
                    </div>
                    <div className='detail-margin-left'>
                        <p className='info-span'>{film.date}</p>
                        <p className='info-span'>{film.rating}</p>
                        {film.age_limit ? (<p className='info-span age-limit'>{film.age_limit}</p>) : ''}
                        <p className='info-span'>{criteriaItem(film.genres)}</p>
                        <p className='info-span'>{criteriaItem(film.directors)}</p>
                        <p className='info-span'>{criteriaItem(film.actors)}</p>
                    </div>
                </div>
                {film.description.length > 10 ?
                    (<>
                        <p className='description'>Описание:</p>
                        <p>{film.description}</p>
                    </>) : null}
            </div>
        </div>
    )
}

export default inject('rootStore')(observer(AboutMovie))
