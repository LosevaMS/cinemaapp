import { observer, inject } from 'mobx-react'
import { useHistory } from 'react-router-dom'
import { RiMovie2Fill } from 'react-icons/ri'
import { BiExit } from 'react-icons/bi'
import { SidebarData } from './NavbarData'
import { useGoogleLogout } from 'react-google-login'
import { doLogout } from '@app/store/UserStore/actions'
import './Navbar.css'
import { clientId } from '@app/constants'

function Navbar({ rootStore }: any) {

    const history = useHistory();

    const onLogoutSuccess = () => {
        doLogout();
        history.push('/login');
    }

    const onFailure = () => {
        console.log('[Logout failed]')
    }

    const { signOut } = useGoogleLogout({
        clientId,
        onLogoutSuccess,
        onFailure
    })

    return (
        <>
            <nav id='nav-menu' className={rootStore.userStore.loggedIn ? 'nav-menu' : 'nav-display-none'}>
                <div className='logo'>
                    <RiMovie2Fill /><h3>Logo</h3>
                </div>
                <ul className='nav-menu-items'>
                    {SidebarData.map((item, index) => {
                        return (
                            <li key={index} className='nav-text'
                                id={(window.location.pathname === '/login' && rootStore.userStore.loggedIn && item.path === '/search') ?
                                    "active" : window.location.pathname == item.path ? "active" : ""}
                                onClick={() => {
                                    window.location.pathname = item.path;
                                }}>
                                <a onClick={() => history.push(item.path)}>
                                    {item.icon}
                                    <span className='nav-item-span'>{item.title}</span>
                                </a>
                            </li>
                        )
                    })}
                </ul>
                <div className='exit' onClick={signOut}>
                    <BiExit /><span>Выход</span>
                </div>
            </nav>
        </>
    )
}

export default inject('rootStore')(observer(Navbar));
