import { FiStar } from 'react-icons/fi'
import { HiSearch, HiOutlineFilm } from 'react-icons/hi'
import { BiLike } from 'react-icons/bi'
import { FiSettings } from 'react-icons/fi'

export const SidebarData = [
    {
        title: 'Избранное',
        path: '/favorites',
        icon: <FiStar />
    },
    {
        title: 'Поиск',
        path: '/search',
        icon: <HiSearch />
    },
    {
        title: 'Рекомендации',
        path: '/recommendations',
        icon: <BiLike />
    },
    {
        title: 'Сейчас в кино',
        path: '/allfilms',
        icon: <HiOutlineFilm />
    },
    {
        title: 'Настройки',
        path: '/settings',
        icon: <FiSettings />
    },
]