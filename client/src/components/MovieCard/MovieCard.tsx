import { useEffect, useState } from 'react'
import { AiFillStar } from 'react-icons/ai'
import { CgMathPlus } from 'react-icons/cg'
import { MdDeleteForever } from 'react-icons/md'
import { Link } from 'react-router-dom'
import './MovieCard.css'
import poster from '@assets/poster.png'
import { postRequest } from '@app/utils'
import { API_URL } from '@app/constants'

const MovieCard = ({ Data, parent, onDelete, user }: any) => {

    const [isFavorite, setFavorite] = useState(Data.isFavorite);

    const criteriaItem = (criteria: string[], isGenre?: boolean) => {
        return (
            <p className={isGenre ? 'film genres' : 'film'}>
                {criteria.map((item: string, index: number) => {
                    if (index === criteria.length - 1) return (`${item}`)
                    else return `${item}, `
                })}</p>
        )
    }

    const onAddBtnClick = (Data: string) => {
        setFavorite(!isFavorite);
        postRequest(API_URL+'/favorites/', { email: user, data: Data })
    }

    return (
        <div className='film-card' id="card">

            <Link to={parent === 'allfilms' ? `allfilms/${Data.alias}` : parent === 'recommendations'
                ? `recommendations/${Data.alias}` : `favorites/${Data.alias}`}>
                <img className='film-poster'
                    src={Data.image ? Data.image : poster}
                    alt='poster' />
            </Link>

            <Link to={parent === 'allfilms' ? `allfilms/${Data.alias}` : parent === 'recommendations'
                ? `recommendations/${Data.alias}` : `favorites/${Data.alias}`}>
                <div className='film-info'>
                    <p className='movie-name'>{Data.name}</p>
                    <p className='film'>{Data.date}</p>
                    {Data.countries ? criteriaItem(Data.countries) : null}
                    {Data.genres ? criteriaItem(Data.genres, true) : null}
                    <span className='rating'>{Data.rating ? Data.rating : null}</span>
                    {Data.rating ? <AiFillStar className='star' /> : null}
                </div>
            </Link >
            {parent !== 'favorites' ? (<>
                {isFavorite ? <MdDeleteForever className='add-icon' onClick={() => {
                    onDelete(Data);
                    if (parent !== 'favorites') setFavorite(!isFavorite)
                }} />
                    : <CgMathPlus className='add-icon' onClick={() => { onAddBtnClick(Data) }} />}
            </>)
                : (<>
                    {isFavorite ? <CgMathPlus className='add-icon' onClick={() => { onAddBtnClick(Data) }} />
                        : <MdDeleteForever className='add-icon' onClick={() => {
                            onDelete(Data);
                            if (parent !== 'favorites') setFavorite(!isFavorite)
                        }} />}
                </>)
            }
        </div >
    )
}

export default MovieCard
