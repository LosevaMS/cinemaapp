import React, { useEffect, useState } from "react";
import "@app/AppStyle.css"
import Navbar from "@app/components/Navbar/Navbar";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import { observer, inject } from 'mobx-react'
import Favorites from "@app/components/Pages/Favorites/Favorites";
import Recommendations from "@app/components/Pages/Recommendations/Recommendations";
import Settings from "@app/components/Pages/Settings/Settings";
import Search from "@app/components/Pages/Search/Search";
import ResultDetail from '@app/components/Pages/Search/Results/ResultDetail/ResultDetail'
import Login from '@app/components/Pages/Login/Login'
import AllFilms from "./components/Pages/AllFilms/AllFilms"
import AboutMovie from '@app/components/AboutMovie/AboutMovie'

const Routers = ({ rootStore }: any) => {

    return (
        <>
            <Router>
                {rootStore.userStore.loggedIn ? (<Navbar />) : (null)}
                <Switch>
                    <Route
                        exact
                        path="/"
                        render={() => {
                            return (
                                rootStore.userStore.loggedIn ?
                                    <Redirect to="/search" /> :
                                    <Redirect to="/login" />
                            )
                        }}
                    />
                    <Route path='/login' component={Login} />
                    <Route exact path='/favorites' component={rootStore.userStore.loggedIn ? Favorites : Login} />
                    <Route exact path='/recommendations' component={rootStore.userStore.loggedIn ? Recommendations : Login} />
                    <Route exact path='/allfilms' component={rootStore.userStore.loggedIn ? AllFilms : Login} />
                    <Route exact path='/search' component={rootStore.userStore.loggedIn ? Search : Login} />
                    <Route path='/settings' component={rootStore.userStore.loggedIn ? Settings : Login} />
                    <Route exact path='/search/:movie' component={rootStore.userStore.loggedIn ? ResultDetail : Login} />
                    <Route exact path='/allfilms/:aboutmovie' component={rootStore.userStore.loggedIn ? AboutMovie : Login} />
                    <Route exact path='/favorites/:aboutmovie' component={rootStore.userStore.loggedIn ? AboutMovie : Login} />
                    <Route exact path='/recommendations/:aboutmovie' component={rootStore.userStore.loggedIn ? AboutMovie : Login} />
                </Switch>
            </Router>
        </>
    );
}

export default inject('rootStore')(observer(Routers))