import axios from 'axios'

export const getRequest = (uri: string, headers?: any) => {
    return axios.get(uri, { headers: headers })
        .then(result => result)
        .catch(error => error)
}
export const postRequest = (uri: string, body:any, headers?: any) => {
    return axios.post(uri, body, { headers: headers })
        .then(result => result)
        .catch(error => error)
}
export const putRequest = (uri: string, body: any, headers?: any) => {
    return axios.put(uri, body, { headers: headers })
        .then(result => result)
        .catch(error => error)
}
export const deleteRequest = (uri: string) => {
    return axios.delete(uri)
        .then(result => result)
        .catch(error => error)
}