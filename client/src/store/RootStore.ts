import { UserStore } from '@app/store/UserStore/UserStore'
import { MoviesStore } from '@app/store/MoviesStore/MoviesStore'

class RootStore {
    userStore: UserStore;
    moviesStore: MoviesStore;

    constructor() {
        this.userStore = new UserStore(this);
        this.moviesStore = new MoviesStore(this);
    }
}

export default new RootStore()