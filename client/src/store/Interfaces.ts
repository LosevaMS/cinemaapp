export interface MovieCardData {
    id: string,
    movie: string,
    date: string,
    genre: string[],
    country: string[],
    src: string,
    rating: string
}