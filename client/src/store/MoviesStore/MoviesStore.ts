import { makeObservable, observable } from 'mobx'
import RootStore from '@app/store/RootStore'
import { MovieCardData } from '@app/store/Interfaces'

export class MoviesStore {
    root: typeof RootStore;
    results: Array<MovieCardData> = [];
    allFilms: Array<MovieCardData> = [];
    criteria: {
        address: [];
        genre: [];
        age_limit: [];
        country: [];
        row: [];
        format: [];
        ticket_number: 0;
        time: {
            start: "08:00";
            end: "23:59";
        };
        rating: [0, 10];
        price: 3000;
        movie: '';
        actors: [];
        date: [];
    } = { address: [],
        genre: [],
        age_limit: [],
        country: [],
        row: [],
        format: [],
        ticket_number: 0,
        time: {
            start: "08:00",
            end: "23:59"
        },
        rating: [0, 10],
        price: 3000,
        movie: '',
        actors: [],
        date: []}

    constructor(root: typeof RootStore) {
        this.root = root;
        makeObservable(this, { results: observable, allFilms: observable, criteria: observable });
    }
}