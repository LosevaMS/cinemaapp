import { runInAction } from 'mobx'
import RootStore from '@app/store/RootStore'

export const setResults = (data: any) => {
    runInAction(() => {
        RootStore.moviesStore.results = data;
    })
}

export const setAllFilms = (data: any) => {
    runInAction(() => {
        RootStore.moviesStore.allFilms = data;
    })
}

export const setCriteria = (data: any) => {
    runInAction(() => {
        console.log(data);
        RootStore.moviesStore.criteria = {
            address: data.address,
            genre: data.genre,
            age_limit: data.age_limit,
            country: data.country,
            row: data.row,
            format: data.format,
            ticket_number: data.ticket_number,
            time: data.time,
            rating: data.rating,
            price: data.price,
            movie: data.movie,
            actors: data.actors,
            date: data.date
        }
    })
}