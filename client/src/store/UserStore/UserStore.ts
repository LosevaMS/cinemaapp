import { makeObservable, observable } from 'mobx'
import RootStore from '@app/store/RootStore'
import { MovieCardData } from '@app/store/Interfaces'

export class UserStore {
    loggedIn = localStorage.getItem('token') ? true : false;
    username = '';
    email = '';
    root: typeof RootStore;
    favorites: Array<MovieCardData> = [];
    recommendations: Array<MovieCardData> = [];

    constructor(root: typeof RootStore) {
        this.root = root;
        makeObservable(this, { loggedIn: observable, favorites: observable, recommendations: observable });
    }
}