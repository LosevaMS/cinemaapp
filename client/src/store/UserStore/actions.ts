import { runInAction } from 'mobx'
import RootStore from '@app/store/RootStore'
import { getRequest, postRequest } from '@app/utils';
import { AUTH_URL, GET_TOKEN_URL } from '@app/constants'

export const doLogin = (e: any, data: any) => {
    e.preventDefault();
    postRequest(GET_TOKEN_URL, JSON.stringify(data), {
        'Content-Type': 'application/json'
    })
        .then(response => {
            runInAction(() => {
                if (response.data.token) {
                    localStorage.setItem('token', response.data.token);
                    RootStore.userStore.loggedIn = true;
                    RootStore.userStore.username = response.data.user.username;
                }
                else { alert('Неверный логин или пароль!') }
            })
        })
        .catch(error => alert('Неверный логин или пароль!'))
}

export const signUp = (e: any, data: any) => {
    e.preventDefault();
    postRequest(AUTH_URL + '/users/', JSON.stringify(data), {
        'Content-Type': 'application/json'
    })
        .then(response => {
            runInAction(() => {
                localStorage.setItem('token', response.data.token);
                RootStore.userStore.loggedIn = true;
                RootStore.userStore.username = response.data.user.username;
            })
        })
}

export const doLogout = () => {
    localStorage.removeItem('token');
    runInAction(() => {
        RootStore.userStore.loggedIn = false;
        RootStore.userStore.username = '';
    })
}

export const doGoogleLogin = (token: string) => {
    localStorage.setItem('token', token);
    runInAction(() => {
        RootStore.userStore.loggedIn = true;
    })
}

export const doReLogin = () => {
    getRequest(AUTH_URL + '/current_user/', {
        Authorization: `JWT ${localStorage.getItem('token')}`
    })
        .then(response => {
            runInAction(() => {
                RootStore.userStore.username = response.data.user.username;
            })
        })
}

export const setRecommendations = (data: any) => {
    runInAction(() => {
        RootStore.userStore.recommendations = data;
    })
}

export const setFavorites = (data: any) => {
    runInAction(() => {
        RootStore.userStore.favorites = data;
    })
}