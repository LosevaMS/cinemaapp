const webpack = require('webpack');
const config = require('./webpack.config');
const WebpackDevServer = require('webpack-dev-server');
const port = 3000;
const host = '0.0.0.0';

const options = {
    publicPath: config.output.publicPath,
    hot: true,
    inline: true,
    contentBase: './templates/client/index.html',
    historyApiFallback: true,
     stats: { colors: true}
};

const server = new WebpackDevServer(webpack(config),options);

server.listen(port,host,function(err){
    if (err){
        console.log(err);
    }
});