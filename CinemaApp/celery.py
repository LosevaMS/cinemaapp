import os
from celery import Celery
from django.conf import settings


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'CinemaApp.settings')

app = Celery('CinemaApp')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.update(
    worker_max_tasks_per_child=1,
    broker_pool_limit=None
)

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

if __name__ == '__main__':
    app.start()
