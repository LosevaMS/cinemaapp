from django.urls import path
from .views import CurrentUserView, UserList

urlpatterns = [
    path('current_user/', CurrentUserView.as_view()),
    path('users/', UserList.as_view())
]