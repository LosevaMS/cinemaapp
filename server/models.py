from django.db import models
from django.contrib.auth.models import User


class Favorite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    image = models.CharField(max_length=255)
    video = models.CharField(max_length=255)
    alias = models.CharField(max_length=255)
    date = models.CharField(max_length=255, blank=True)
    countries = models.CharField(max_length=255)
    actors = models.CharField(max_length=255)
    directors = models.CharField(max_length=255)
    genres = models.CharField(max_length=255)
    rating = models.CharField(max_length=255, blank=True)
    ageLimit = models.CharField(max_length=255)
    description = models.CharField(max_length=1000)

    def __str__(self):
        return self.name


