from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK
from rest_framework import permissions, generics
import math
from rest_framework.response import Response
import json
import numpy
import sys
import yaml
from datetime import datetime
import os
from server.models import Favorite
from django.contrib.auth.models import User
from server.serializers import FavoriteSerializer
from django.core import serializers


date_dict = {
    'январь': '01', 'февраль': '02', 'март': '03', 'апрель': '04', 'май': '05', 'июнь': '06',
    'июль': '07', 'август': '08', 'сентябрь': '09', 'октябрь': '10', 'ноябрь': '11', 'декабрь': '12',
    'января': '01', 'февраля': '02', 'марта': '03', 'апреля': '04', 'мая': '05', 'июня': '06',
    'июля': '07', 'августа': '08', 'сентября': '09', 'октября': '10', 'ноября': '11', 'декабря': '12'
}


class SearchView(generics.GenericAPIView):

    permission_classes = [permissions.AllowAny]

    def __init__(self):

        self.number = 0
        self.path = os.path.join(sys.path[0], 'afisha.json')

    def search(self, source, criteria):

        movies = {}

        for elem in source:

            coincidences = 0
            matched = []

            for value in ['genre', 'country']:
                if criteria.get(value) is not None \
                        and True in numpy.in1d([i.lower() for i in elem[value]],
                                               [i['value'].lower() for i in criteria[value]]):
                    coincidences += 1
                    matched.append(value)

            for value in ['age_limit', 'address']:
                if criteria.get(value) is not None and elem[value].lower() \
                        in [i['value'].lower() for i in criteria[value]]:
                    coincidences += 1
                    matched.append(value)

            if criteria.get('movie') is not None and criteria['movie'].lower() in elem['name'].lower():
                coincidences += 1
                matched.append('movie')

            if criteria.get('actors') is not None and True in [x in y for x in criteria['actors'] for y in
                                                               elem['actors']]:
                coincidences += 1
                matched.append('actors')

            if elem['rating'] != "" and criteria['rating'][0] <= float(elem['rating']) <= criteria['rating'][1]\
                    or elem['rating'] == "":
                coincidences += 1
                if elem['rating'] != "":
                    matched.append('rating')

            if criteria.get('price') is not None:
                if elem['price'] != '' and criteria.get('price') is not None and criteria['price'] >= float(
                        elem['price']):
                    coincidences += 1
                else:
                    continue

            if criteria.get('time') is not None:
                if datetime.strptime(criteria['time']['start'], '%H:%M') <= datetime.strptime(elem['time'], '%H:%M') \
                        <= datetime.strptime(criteria['time']['end'], '%H:%M'):
                    coincidences += 1
                else:
                    continue

            if criteria.get('format') is not None:
                if True in numpy.in1d([i.lower() for i in elem['format']],
                                      [i['value'].lower() for i in criteria['format']]):
                    coincidences += 1
                else:
                    continue

            dateList = elem['date'].split(' ')
            dateList[1] = date_dict[dateList[1]]

            dateCheck = '-'.join(dateList)

            if criteria.get('date') is not None:
                if criteria['date'] == dateCheck:
                    coincidences += 1
                else:
                    continue

            if len(dateList[0]) == 1:
                dateList[0] = '0' + dateList[0]

            currentTime = datetime.now()
            elem['date'] = '.'.join(dateList)

            if datetime.strptime(f'{elem["date"]} {elem["time"]}', '%d.%m.%Y  %H:%M') < currentTime:
                continue

            if coincidences >= math.ceil(self.number * 4 / 10):

                name = elem['name']
                alias = elem['link'].replace('https://kassa.rambler.ru/nnovgorod/', '').replace('/', '')

                if name not in movies:

                    movies[name] = {'genre': elem['genre'], 'age_limit': elem['age_limit'], 'actors': elem['actors'],
                                    'country': elem['country'], 'rating': elem['rating'], 'image': elem['image'],
                                    'satisfied': matched, 'coincidences': coincidences, 'movie': name, 'alias': alias,
                                    'status': '', 'matches': f'{coincidences}/{self.number}',
                                    'directors': elem['directors'], 'duration': elem['duration'],
                                    'sessions': [{'cinema': elem['cinema'], 'address': elem['address'],
                                                  'schedule': [{'date': elem['date'], 'time': elem['time'],
                                                                'price': elem['price'], 'format': elem['format'][0],
                                                                'url': elem['url']}]}]}
                else:
                    if coincidences > movies[name]['coincidences']:
                        movies[name]['coincidences'] = coincidences
                        movies[name]['satisfied'] = matched
                        movies[name]['matches'] = f'{coincidences}/{self.number}'
                    if elem['cinema'] not in [session['cinema'] for session in movies[name]['sessions']]:
                        newSession = {'cinema': elem['cinema'], 'address': elem['address'],
                                      'schedule': [{'date': elem['date'], 'time': elem['time'],
                                                    'price': elem['price'], 'format': elem['format'][0],
                                                    'url': elem['url']}]}
                        movies[name]['sessions'].append(newSession)
                    else:
                        for oldSession in movies[name]['sessions']:
                            if oldSession['cinema'] == elem['cinema']:
                                newSchedule = {'date': elem['date'], 'time': elem['time'],
                                               'price': elem['price'], 'format': elem['format'][0], 'url': elem['url']}
                                oldSession['schedule'].append(newSchedule)

        for movie in movies:
            for session in movies[movie]['sessions']:
                session['schedule'].sort(key=lambda dictionary: (dictionary['date'], dictionary['time']))

        arrayResults = [i for i in movies.values()]

        arrayResults.sort(key=lambda dictionary: dictionary['coincidences'], reverse=True)

        if len(arrayResults) == 1:
            arrayResults[0]['status'] = 'gold'
        if len(arrayResults) > 1:
            if arrayResults[0]['coincidences'] > arrayResults[1]['coincidences']:
                arrayResults[0]['status'] = 'gold'
                if len(arrayResults) > 2:
                    if arrayResults[1]['coincidences'] > arrayResults[2]['coincidences']:
                        arrayResults[1]['status'] = 'silver'
                        if len(arrayResults) > 3:
                            if arrayResults[2]['coincidences'] > arrayResults[3]['coincidences']:
                                arrayResults[2]['status'] = 'bronze'

        return arrayResults

    def get(self, request, *args, **kwargs):

        criteria = yaml.load(request.GET.get('data'))
        print(criteria)

        with open(self.path) as f:
            afisha_json = json.load(f)

        deleted_keys = []

        for key in criteria.keys():
            value = criteria[key]
            if type(value) in (tuple, list) and len(value) != 0 or type(value) not in (tuple, list) and value != 0 \
                    and value != '':
                self.number += 1
            else:
                deleted_keys.append(key)

        for key in deleted_keys:
            criteria.pop(key)

        results = self.search(afisha_json, criteria)

        return Response({'message': 'Success', 'results': results})


class AllFilmsView(generics.GenericAPIView):

    permission_classes = [permissions.AllowAny]

    def __init__(self):

        self.path = os.path.join(sys.path[0], 'allfilms.json')

    def get(self, request, *args, **kwargs):

        email = request.GET.get('email')

        if email == '':
            return Response({'message': 'Success', 'allfilms': []})

        allfilms = []

        currentUser = User.objects.filter(username=email)[0]

        with open(self.path) as f:
            allfilms_json = json.load(f)

        for elem in allfilms_json:

            if len(Favorite.objects.filter(user=currentUser, name=elem['name'])) != 0:
                elem['isFavorite'] = True
            else:
                elem['isFavorite'] = False

            allfilms.append(elem)

        if allfilms_json is not None:
            return Response({'message': 'Success', 'allfilms': allfilms})


class RecommendationsView(generics.GenericAPIView):

    permission_classes = [permissions.AllowAny]

    def __init__(self):

        self.path = os.path.join(sys.path[0], 'premiers.json')
        self.recommendations = []

    def analyzeFavorites(self, email):

        genres = {}
        actors = {}
        directors = {}
        ageLimits = {}
        countries = {}

        if len(User.objects.filter(username=email)) != 0:

            currentUser = User.objects.filter(username=email)[0]

            favoriteFilms = Favorite.objects.filter(user=currentUser)

            number = math.ceil(len(favoriteFilms) / 4)

            for film in favoriteFilms:

                for genre in film.genres.replace(' ', '').split(','):
                    if genre not in genres.keys():
                        genres[genre] = 1
                    else:
                        genres[genre] += 1

                for country in film.countries.split(','):
                    if country not in countries.keys():
                        countries[country] = 1
                    else:
                        countries[country] += 1

                for actor in film.actors.split(','):
                    if actor not in actors.keys():
                        actors[actor] = 1
                    else:
                        actors[actor] += 1

                for director in film.directors.split(','):
                    if director not in directors.keys():
                        directors[director] = 1
                    else:
                        directors[director] += 1

                if film.ageLimit not in ageLimits.keys():
                    ageLimits[film.ageLimit] = 1
                else:
                    ageLimits[film.ageLimit] += 1

        return {'actors': [key for key in actors.keys() if actors[key] >= number],
                'directors': [key for key in directors.keys() if directors[key] >= number],
                'genres': [key for key in genres.keys() if genres[key] >= number],
                'countries': [key for key in countries.keys() if countries[key] >= number],
                'age_limit': [key for key in ageLimits.keys() if ageLimits[key] >= number]}

    def recommend(self, source, preferences, email):

        favoriteItems = self.analyzeFavorites(email)

        for elem in source:

            coincidences = 0

            for value in ['genres', 'countries', 'actors', 'directors']:
                if preferences[value] is not None \
                        and True in numpy.in1d([i.lower() for i in elem[value]],
                                               [i.lower() for i in preferences[value]+favoriteItems[value]]):
                    coincidences += 1

            if preferences.get('age_limit') is not None and elem['age_limit'].lower() \
                    in [i.lower() for i in preferences['age_limit']+favoriteItems['age_limit']]:
                coincidences += 1

            if coincidences >= 3:
                self.recommendations.append(elem)

    def get(self, request, *args, **kwargs):

        preferences = yaml.load(request.GET.get('preferences'))
        email = request.GET.get('email')

        if email == '':
            return Response({'message': 'Success', 'recommendations': []})

        with open(self.path) as f:
            premiers_json = json.load(f)

        self.recommend(premiers_json, preferences, email)

        currentUser = User.objects.filter(username=email)[0]

        for elem in self.recommendations:
            if len(Favorite.objects.filter(user=currentUser, name=elem['name'])) != 0:
                elem['isFavorite'] = True
            else:
                elem['isFavorite'] = False

        return Response({'message': 'Success', 'recommendations': self.recommendations})


class FavoritesView(generics.GenericAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = FavoriteSerializer

    def post(self, request, *args, **kwargs):

        data = request.data['data']
        email = request.data['email']

        currentUser = User.objects.filter(username=email)[0]
        favourites = Favorite.objects.filter(user=currentUser, name=data['name'])

        if len(favourites) == 0:
            newElem = Favorite()

            newElem.user = currentUser
            newElem.name = data['name']
            newElem.image = data['image']
            newElem.video = data['video']
            newElem.alias = data['alias']
            newElem.ageLimit = data['age_limit']
            newElem.actors = ','.join(data['actors'])
            newElem.directors = ','.join(data['directors'])
            newElem.countries = ','.join(data['countries'])
            if data.get('date'):
                newElem.date = data['date']
            else:
                newElem.date = ''
            newElem.description = data['description']
            newElem.genres = ','.join(data['genres'])
            if data.get('rating'):
                newElem.rating = data['rating']
            else:
                newElem.rating = ''

            newElem.save()

        return Response({'message': 'Success'}, status=HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):

        data = yaml.load(request.GET.get('data'))
        email = request.GET.get('email')

        currentUser = User.objects.filter(username=email).first()

        favorite = Favorite.objects.get(user=currentUser, name=data['name'])
        favorite.delete()

        return Response({'message': 'Success'}, status=HTTP_200_OK)

    def get(self, request, *args, **kwargs):

        email = request.GET.get('email')

        currentUser = User.objects.filter(username=email).first()

        favorites = Favorite.objects.filter(user=currentUser)

        results = []

        for favorite in favorites:
            favoriteJson = json.loads(serializers.serialize('json', [favorite]))[0]['fields']
            favoriteJson['actors'] = favoriteJson['actors'].split(',')
            favoriteJson['directors'] = favoriteJson['directors'].split(',')
            favoriteJson['genres'] = favoriteJson['genres'].split(',')
            favoriteJson['countries'] = favoriteJson['countries'].split(',')

            results.append(favoriteJson)

        return Response({'message': 'Success', 'favorites': results}, status=HTTP_200_OK)
