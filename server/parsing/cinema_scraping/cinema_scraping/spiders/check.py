from datetime import datetime


date_dict = {
    'январь': '01', 'февраль': '02', 'март': '03', 'апрель': '04', 'май': '05', 'июнь': '06',
    'июль': '07', 'август': '08', 'сентябрь': '09', 'октябрь': '10', 'ноябрь': '11', 'декабрь': '12',
    'января': '01', 'февраля': '02', 'марта': '03', 'апреля': '04', 'мая': '05', 'июня': '06',
    'июля': '07', 'августа': '08', 'сентября': '09', 'октября': '10', 'ноября': '11', 'декабря': '12'
}
currentTime = datetime.now()
currentYear = str(currentTime.year)


def checkDate(date, comparison):

    if date is not None:

        dateList = date.split(' ')

        if dateList[-1] == currentYear:

            if len(dateList) == 2:

                dateList[0] = date_dict[dateList[0]]
                dateFormat = datetime.strptime(' '.join(dateList), '%m %Y')

            if len(dateList) == 3:

                dateList[1] = date_dict[dateList[1]]
                dateFormat = datetime.strptime(' '.join(dateList), '%d %m %Y')

            if comparison == 'less':
                if dateFormat > currentTime:
                    return False
            else:
                if dateFormat <= currentTime:
                    return False

    return True
