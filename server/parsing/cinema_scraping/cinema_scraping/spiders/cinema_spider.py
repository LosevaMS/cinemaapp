import scrapy
from scrapy.crawler import CrawlerProcess
from datetime import datetime
import json
import sys
import time
import os
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


ROOT_DIR = sys.path[0]
current_year = str(datetime.now().year)
ratings = {}

pathDriver = ROOT_DIR + '/chromedriver'


def getUrls(movies):

    urls = []

    for name in movies.keys():

        op = webdriver.ChromeOptions()
        op.add_argument('headless')

        driver = webdriver.Chrome(executable_path=pathDriver, options=op)

        driver.get('https://kassa.rambler.ru/nnovgorod/movie')

        textarea = driver.find_element_by_xpath('//*[@id="mainpage-search"]')
        textarea.send_keys(name)

        time.sleep(2)

        button = driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div/header/section[1]/div/form/fieldset/label')
        button.click()

        time.sleep(2)

        currentUrl = driver.current_url

        if 'movie/' in currentUrl:

            urls.append(currentUrl)

            movieName = driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div/div/section[1]/div[2]/div[1]/h1').text

            if movies[name] is not None:
                ratings[movieName] = movies[name]
            else:
                ratings[movieName] = ""

        else:
            print('Не найдено', name)

        driver.quit()

    return urls


class CinemaSpider(scrapy.Spider):
    name = 'cinema_spider'

    def time_parse(self, response):

        results = []

        date = response.css('li.date_item.active').css('time.date_item__time::text').get() + ' ' + current_year

        numberSession = 0

        for cinemaCard in response.css('div.rasp_item'):
            cinema = cinemaCard.css('span.s-name::text').get()
            address = cinemaCard.css('span[itemprop=streetAddress]::text').get()

            for i in range(len(cinemaCard.css('div.rasp_type'))):
                format = cinemaCard.css('div.rasp_type::text')[i].get().split(' от')[0].replace('Сеансы ', '').\
                    replace('на языке оригинала с русскими субтитрами', 'en').replace('Dolby Atmos', 'Atmos').split(',')

                sessions = cinemaCard.css('ul.rasp_time')[i].css('li.btn_rasp')

                for session in sessions:

                    if len(session.css('.inactive')) != 0:
                        numberSession += 1
                        continue

                    timeSession = session.css('*::text').get().replace(' ', '').replace('\n', '')

                    try:
                        price = session.css('meta[itemprop=lowPrice]').attrib['content']

                        op = webdriver.ChromeOptions()
                        op.add_argument('headless')

                        driver = webdriver.Chrome(executable_path=pathDriver, options=op)
                        driver.get(response.request.url)

                        driver.set_window_size(50, 50)

                        button = driver.find_elements_by_css_selector('li.btn_rasp')[numberSession]
                        button.click()

                        while True:
                            time.sleep(1)
                            url = driver.current_url
                            if '?date=' not in url:
                                break

                        try:
                            driver.find_element_by_xpath('/html/body/div/div[2]/div/div/div[1]')
                            print('Ошибка.', response.meta['name'], date, cinema, time, url)

                        except NoSuchElementException:

                            results.append({'name': response.meta['name'], 'image': response.meta['image'],
                                            'age_limit': response.meta['age_limit'], 'link': response.meta['link'],
                                            'url': url, 'time': timeSession, 'price': price, 'format': format,
                                            'country': response.meta['country'], 'rating': response.meta['rating'],
                                            'genre': response.meta['genre'], 'actors': response.meta['actors'],
                                            'duration': response.meta['duration'], 'cinema': cinema, 'date': date,
                                            'directors': response.meta['directors'], 'address': address})

                    except KeyError:

                        print('Билеты закончились.', response.meta['name'], date, cinema, time)

                    driver.quit()

                    numberSession += 1

        return results

    def parse(self, response):

        name = response.css('h1.item_title::text').get()
        genres = response.css('h3.item_title3::text').get().replace('\n', '').replace('\t', '').split(',')[:-1]
        age_limit = response.css('h3.item_title3::text').get().replace('\n', '').replace('\t', '').split(',')[-1]

        image = response.xpath('/html/body/div[2]/div[2]/div/div/div/section[1]/div[1]/img').xpath('@src').extract_first()
        if image is None:
            image = ''

        duration = ''
        actors = []
        directors = []
        country = []

        for item in response.css('div.item_peop'):
            if item.css('span.dt::text').get() == 'Длительность:':
                duration = item.css('span.dd::text').get()
            if item.css('span.dt::text').get() == 'Производство:':
                country = item.css('span.dd::text').get().split(',')
            if item.css('span.dt::text').get() == 'Режиссёр:':
                directors = item.css('span.dd::text').get().split(',')
            if item.css('span.dt::text').get() == 'Актеры:':
                actors = item.css('span.item_peop__actors::text').get().split(',')

        links = response.css('span.date_link::attr(data-url)').extract()

        for dateLink in links:

            link = 'https://kassa.rambler.ru' + dateLink
            movieLink = link.split('?date')[0]

            yield scrapy.Request(link, callback=self.time_parse, meta={'name': name, 'rating': ratings[name],
                                                                       'link': movieLink, 'country': country,
                                                                       'age_limit': age_limit, 'image': image,
                                                                       'actors': actors, 'genre': genres,
                                                                       'duration': duration, 'directors': directors})


def parsingAllSessions():

    start_time = time.time()

    path = os.path.join(ROOT_DIR, 'allfilms.json')

    with open(path) as f:
        allfilms = json.load(f)

    movies = {}

    for film in allfilms:
        movies[film['name'].replace('ONP: ', '')] = film['rating']

    urls = getUrls(movies)

    pathAfisha = os.path.join(ROOT_DIR, 'afisha.json')

    if os.path.isfile(pathAfisha):
        f = open(pathAfisha, "r+")
        f.seek(0)
        f.truncate()

    process = CrawlerProcess(settings={
        "FEEDS": {
            pathAfisha: {"format": "json"},
        },
    })

    process.crawl(CinemaSpider, start_urls=urls)
    process.start()

    with open(pathAfisha) as f:
        afisha = json.load(f)

    print(len(afisha))

    print('spent time:', time.time() - start_time)
