import scrapy
from scrapy.crawler import CrawlerProcess
import os
import time
import json
import sys
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from .check import checkDate


ROOT_DIR = sys.path[0]


class AllFilmsSpider(scrapy.Spider):

    name = 'allfilms_spider'
    start_urls = ['https://www.afisha.ru/nnovgorod/schedule_cinema/na-mesyac/?view=list']

    def __init__(self):

        self.pathDriver = ROOT_DIR + '/chromedriver'

    def findImage(self, movieName):

        op = webdriver.ChromeOptions()
        op.add_argument('headless')

        driver = webdriver.Chrome(executable_path=self.pathDriver, options=op)

        driver.get("https://www.google.ru")

        textarea = driver.find_element_by_xpath('/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input')

        textarea.send_keys(movieName + " фильм постер\n")

        time.sleep(2)

        try:

            if driver.find_element_by_xpath('//*[@id="hdtb-msb"]/div[1]/div/div[2]/a').text == 'Картинки':
                button = driver.find_element_by_xpath('//*[@id="hdtb-msb"]/div[1]/div/div[2]/a')
            else:
                button = driver.find_element_by_xpath('//*[@id="hdtb-msb"]/div[1]/div/div[3]/a')
            button.click()

            time.sleep(2)

            button = driver.find_element_by_xpath('//*[@id="islrg"]/div[1]/div[1]/a[1]/div[1]/img')
            button.click()

            time.sleep(3)

            image = driver.find_element_by_xpath(
                '//*[@id="Sva75c"]/div/div/div[3]/div[2]/c-wiz/div/div[1]/div[1]/div/div[2]/a/img').get_attribute("src")

        except NoSuchElementException:
            image = ''

        driver.quit()

        return image

    def page_parse(self, response):

        age_limit = response.css('div._2ahFx::text').get()

        video = response.css('video._1t9wi::attr(src)').get()

        if video is None:
            video = ''

        descriptions = response.css("div.restrict-text__wrapper::text").getall()

        if len(descriptions) != 0:
            description = descriptions[-1]
        else:
            description = ''

        date = ''

        for block in response.css('div._3STyo'):
            if block.css('div._2ahFx::text').get() == 'Дата выхода в России':
                date = block.css('div:nth-child(2)::text').get()

        if not checkDate(date, 'less'):
            return

        image = self.findImage(response.meta['name'])

        return {'name': response.meta['name'], 'alias': response.meta['alias'], 'rating': response.meta['rating'],
                'image': image, 'video': video, 'date': date, 'countries': response.meta['countries'],
                'age_limit': age_limit, 'actors': response.meta['actors'], 'directors': response.meta['directors'],
                'genres': response.meta['genres'], 'description': description}

    def parse(self, response):

        for movie_card in response.css('div.new-list__item.movie-item'):
            if movie_card.css('div.new-list__item-status::text')[0].get().isdigit() and len(movie_card.css('div.new-list__item-status::text')) > 1:
                countries = movie_card.css('div.new-list__item-status::text')[2].get().split(',')
            else:
                countries = movie_card.css('div.new-list__item-status::text')[0].get().split(',')

            link = 'https://www.afisha.ru' + movie_card.css('a.new-list__item-link').attrib['href']
            name = movie_card.css('a.new-list__item-link::text').get()
            rating = movie_card.css('div.rating-static__item.rating-badge::text').get()

            for new_list in movie_card.css('div.new-list__item-record'):
                if new_list.css('div.new-list__item-record-label::text').get() == 'жанр:':
                    genres = new_list.css('a._1OSu0::text').getall()
                elif new_list.css('div.new-list__item-record-label::text').get() == 'режиссер:':
                    directors = new_list.css('a._1OSu0::text').getall()
                elif new_list.css('div.new-list__item-record-label::text').get() == 'актеры:':
                    actors = new_list.css('a._1OSu0::text').getall()

            yield scrapy.Request(link, callback=self.page_parse,
                                 meta={'name': name, 'rating': rating, 'link': link,
                                       'alias': link.replace('https://www.afisha.ru/', '').replace('/', ''),
                                       'countries': countries, 'actors': actors, 'genres': genres,
                                       'directors': directors})

        next_btn = response.css('div._1iwot._1KpTl')

        if next_btn is not None:
            link = 'https://www.afisha.ru' + \
                response.css('a._2jLXO')[-1].attrib['href']
            yield response.follow(link, callback=self.parse)


def parsingAllFilms():
    start_time = time.time()

    path = os.path.join(ROOT_DIR, 'allfilms.json')

    if os.path.isfile(path):
        f = open(path, "r+")
        f.seek(0)
        f.truncate()

    process = CrawlerProcess(settings={
        "FEEDS": {
            path: {"format": "json"},
        },
    })

    process.crawl(AllFilmsSpider)
    process.start()

    with open(path) as f:
        allfilms = json.load(f)

    print(len(allfilms))

    print('spent time:', time.time() - start_time)
