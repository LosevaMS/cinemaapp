import scrapy
from scrapy.crawler import CrawlerProcess
import os
import time
import json
import sys
from datetime import datetime
from .check import checkDate


ROOT_DIR = sys.path[0]
currentYear = str(datetime.now().year)


class PremiersSpider(scrapy.Spider):
    name = 'premiers_spider'
    start_urls = [f"https://www.kinoafisha.info/releases/{currentYear}/"]

    def page_parse(self, response):

        description = response.xpath('//p/text()').extract_first()

        info = response.css('div.filmInfo_infoItem')
        date = age_limit = ''

        for elem in info:
            infoName = elem.css('span.filmInfo_infoName::text').get()

            if infoName == 'Премьера в России':
                date = elem.css('span.filmInfo_infoData::text').get()
            elif infoName == 'Возраст':
                age_limit = elem.css('span.filmInfo_infoData::text').get()

        if not checkDate(date, 'more'):
            return

        infoActors = response.css('div.filmInfoActors_item')
        directors = actors = []

        for elem in infoActors:
            title = elem.css('span.filmInfoActors_title::text').get()

            if title == 'Режиссер':
                directors = [i[1:] if i[0] == ' ' else i for i in elem.css('span.badgeList_name::text').getall()]
            elif title == 'В ролях':
                actors = [i[1:] if i[0] == ' ' else i for i in  elem.css('span.badgeList_name::text').getall()]

        return {'name': response.meta['name'], 'image': response.meta['image'], 'alias': response.meta['alias'],
                'date': date, 'age_limit': age_limit, 'directors': directors, 'actors': actors, 'video': '',
                'genres': response.meta['genres'], 'countries': response.meta['countries'], 'description': description}

    def parse(self, response):

        number = round(len(response.css('div.movieList_item'))/4)

        for movie_card in response.css('div.movieList_item')[:number]:

            name = movie_card.css('span.movieItem_title::text').get()
            countries = [i[1:] if i[0] == ' ' else i for i in
                         movie_card.css('span.movieItem_year::text').get().split(',')[1].split('/')]
            link = movie_card.css('a.movieItem_ref').attrib['href']

            genres = movie_card.css('span.movieItem_genres::text').get()
            if genres is not None:
                genres = [i.title() for i in genres.replace(' ', '').split(',')]
                for i in range(len(genres)):
                    if genres[i] == 'Анимация':
                        genres[i] = 'Мультфильм'
                    elif genres[i] == 'Приключения':
                        genres[i] = 'Приключение'
                    elif genres[i] == 'Криминал':
                        genres[i] = 'Криминальный'
                    elif 'Музыка' in genres[i]:
                        genres[i] = 'Музыкальный'
            else:
                genres = ''

            image = movie_card.css('img.picture_image')
            if 'data-picture' in image.attrib:
                image = image.attrib['data-picture']
            else:
                image = ''

            yield scrapy.Request(link, callback=self.page_parse,
                                 meta={'name': name, 'genres': genres, 'countries': countries, 'image': image,
                                       'alias': link.replace('https://www.kinoafisha.info/', '').replace('/', '').replace('movies', 'movie')})


def parsingPremiers():
    start_time = time.time()

    path = os.path.join(ROOT_DIR, 'premiers.json')

    if os.path.isfile(path):
        f = open(path, "r+")
        f.seek(0)
        f.truncate()

    process = CrawlerProcess(settings={
        "FEEDS": {
            path: {"format": "json"},
        },
    })

    process.crawl(PremiersSpider)
    process.start()

    with open(path) as f:
        premiers = json.load(f)

    print(len(premiers))

    print('spent time:', time.time() - start_time)
