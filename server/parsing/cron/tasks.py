from server.parsing.cinema_scraping.cinema_scraping.spiders.allfilms_spider import parsingAllFilms
from server.parsing.cinema_scraping.cinema_scraping.spiders.premiers_spider import parsingPremiers
from server.parsing.cinema_scraping.cinema_scraping.spiders.cinema_spider import parsingAllSessions
from celery.task import periodic_task
from celery.schedules import crontab


@periodic_task(run_every=(crontab(minute=1, hour=21)), name='allFilmsParsing')
def allFilmsParsing():
    parsingAllFilms()


@periodic_task(run_every=(crontab(minute=1, hour=21)), name='premiersParsing')
def premiersParsing():
    parsingPremiers()


@periodic_task(run_every=(crontab(minute=1, hour=21)), name='allSessionsParsing')
def allSessionsParsing():
    parsingAllSessions()
