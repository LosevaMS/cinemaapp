from .views import SearchView, AllFilmsView, RecommendationsView, FavoritesView
from django.urls import path, re_path
from django.contrib import admin

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^api/results/$', SearchView.as_view()),
    re_path(r'^api/allfilms/$', AllFilmsView.as_view()),
    re_path(r'^api/recommendations/$', RecommendationsView.as_view()),
    re_path(r'^api/favorites/$', FavoritesView.as_view())]

