# Generated by Django 3.1.7 on 2021-02-22 08:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0003_auto_20210219_2303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='request',
            name='age_limit',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='rating',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
