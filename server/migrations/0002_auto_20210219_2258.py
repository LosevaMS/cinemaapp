# Generated by Django 3.1.7 on 2021-02-19 19:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='request',
            name='age_limit',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='rating',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
