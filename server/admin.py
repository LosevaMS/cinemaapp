from django.contrib import admin
from .models import Favorite


class FavoriteAdmin(admin.ModelAdmin):
    list_display = ('user', 'name', 'date', 'genres', 'rating')


admin.site.register(Favorite, FavoriteAdmin)
